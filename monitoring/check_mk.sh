#!/bin/bash

. /var/www/django/frms/env/bin/activate
/var/www/django/frms/app/manage.py check_health -v2 >/dev/null 2>&1
if [ $? -eq 0 ]
then
        echo "0 saxidapi dummy=0;0;0;0; OK - SaxID API Status"
else
        echo "2 saxidapi dummy=0;0;0;0; CRIT - SaxID API Status"
fi
