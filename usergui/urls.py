# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url
from usergui import views

urlpatterns = [
    url('^$', views.index, name='index'),
    url('^detail/(?P<uuid>[^/]+)/$', views.detail, name="detail"),
]
app_name = 'usergui'
