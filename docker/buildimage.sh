#!/usr/bin/env bash
docker pull debian:buster
datum=$(date +%Y-%m-%d)
docker build -t frms:${datum} .
docker build -t frms:latest .
