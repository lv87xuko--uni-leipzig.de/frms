Betrieb mit Docker
==================

Image bauen
-----------

automatisch: 
```bash
    buildimage.sh
```

Container starten
-----------------

* Es muss eine MariaDB oder PostgreSQL Datenbank vorhanden sein
* Es muss eine Minimalkonfiguration erstellt werden. Als Vorlage gibt es
  * [docker/example-settings/postgresql/settings_docker.py]
  * [docker/example-settings/mariadb/settings_docker.py]
  Für die Konfiguration kann ein beliebiges Verzeichnis auf dem Hostsystem gewählt werden, das gebackupt wird.
* Apache mit SSL muss eingerichtet sein. Außerdem ist [mod_proxy_uwsgi](https://httpd.apache.org/docs/2.4/mod/mod_proxy_uwsgi.html) notwendig. Der UWSGI im Container lauscht auf Port 8000 mit UWSGI Protokoll.
* Beim Start des Containers werden Datenbankmigrationen durchgeführt und die Datei `django_secret.txt` im Konfigurationsverzeichnis angelegt, wenn noch nicht vorhanden. Dort wird das Django Secret gespeichert, mit dem Cookies verschlüsselt werden. Bei Upgrade Installationen, die von Nicht-Docker-Deployment zu Docker-Deployment migrieren, muss der Wert des Django Secret übernommen werden (siehe unten).

Angenommen, die Konfiguration befindet sich unter `/etc/frms`, dann sollte der Container folgendermaßen gestartet werden:
```bash
    docker run --name frms --rm -v /etc/frms/:/var/www/django/frms/private/ -d -p 127.0.0.1:8000:8000 frms:latest
```

Im Apache reicht dann folgende Konfiguration im passenden Virtualhost:

```
    ProxyPass /api uwsgi://127.0.0.1:8000/frms
    ProxyPass /static uwsgi://127.0.0.1:8000/frms/static
```

Testen der Software in Containern mit mehrere Instanzen
-------------------------------------------------------

Die Container können gleich mit zum Testen der Funktionalität genutzt werden. Dazu muss lokal ein passender Container gebaut werden und die Tests gestartet werden.
Zur Ausführung der Tests wird docker-compose genutzt.

~~~bash
./buildimage.sh
./test.sh
~~~

Migration von mod_wsgi Deployment zu Container Deployment
---------------------------------------------------------

* Konfiguration für SaxID Instanz in Docker Containern muss in einem Verzeichnis liegen. Es gibt darin dann 2 Dateien:
  * `settings_docker.py` hier sind die Django Settings. Muss gültiger Python Code sein
  * `django_secret.txt` hier steht das Secret drin. Es muss der pure String sein, ohne Anführungszeichen etc.
* Config übernehmen und anpassen . Es sind die Werte aus /var/www/django/frms/private/settings_local.py zu übernehmen. Abgucken in den Beispielen:
  * [docker/example-settings/postgresql/settings_docker.py]
  * [docker/example-settings/mariadb/settings_docker.py]
* den Inhalt der Variable SECRET_KEY aus den alten Settings in die Datei django_secret.txt ablegen
* Apache Config anpassen:
  ~~~
  LoadModule proxy_uwsgi_module modules/mod_proxy_uwsgi.so
  ProxyPass /api uwsgi://localhost:8000/frms
  ProxyPass /static uwsgi://localhost:8000/frms/static
  ~~~
* Wichtig ist, dass /api/admin geschützt ist, z.B. per Basic Auth oder Shibboleth
* Die bestehende Datenbank kann weitergenutzt werden.

## Beispiel alte Konfigurationsdateien

`/var/www/django/frms/private/settings_private.py` mit folgendem Inhalt:
~~~python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

SECRET_KEY = "XYZ..." #60 Stellen Zufall für Django
DATABASE_PASSWORD = "XXXXX"
~~~

`/var/www/django/frms/private/settings_local.py` mit folgendem Inhalt:
~~~python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from settings_private import *
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'saxid',
        'USER': 'saxid_rw',
        'PASSWORD': DATABASE_PASSWORD,
        'HOST': 'postgresql-host.hs-mittweida.de',
    },
}
# Der Realm vom EPPN der eigenen Nutzer
SERVER_REALM="hs-mittweida.de"
HTTP_SERVER_NAME="https://saxid-api.hs-mittweida.de"
ALLOWED_HOSTS = ["saxid-api.hs-mittweida.de"]
STATIC_URL  = '/static/api/'
AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.RemoteUserBackend',
]
~~~

## Neue Config
`/etc/frms/django_secret.txt`
~~~
XYZ...
~~~

`/etc/frms/settings_docker.py`
~~~python
SERVER_REALM="hs-mittweida.de"
HTTP_SERVER_NAME="https://saxid-api.hs-mittweida.de"
ALLOWED_HOSTS = ["saxid-api.hs-mittweida.de"]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'saxid',
        'USER': 'saxid_rw',
        'PASSWORD': 'XXXXX',
        'HOST': 'postgresql-host.hs-mittweida.de',
    },
}
~~~
