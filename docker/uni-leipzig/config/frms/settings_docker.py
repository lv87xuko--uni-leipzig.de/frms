# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function
import os

SERVER_REALM = "uni-leipzig.de"  # hs-mittweida.de
HTTP_SERVER_NAME = "https://saxid.uni-leipzig.de"  # https://saxid-api.hs-mittweida.de
ALLOWED_HOSTS = ["*"]  # saxid-api.hs-mittweida.de

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'frms',
        "USER": os.environ.get("POSTGRES_USER"),
        "PASSWORD": os.environ.get("POSTGRES_PASSWORD"),
        'HOST': 'db',
    }
}
