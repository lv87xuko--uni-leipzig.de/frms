# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function


SERVER_REALM="a.edu"     # hs-mittweida.de
HTTP_SERVER_NAME="https://saxid.a.edu" # https://saxid-api.hs-mittweida.de
ALLOWED_HOSTS = ["saxid.a.edu"]   # saxid-api.hs-mittweida.de

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'datenbank',
        'USER': 'datenbank_rw',
        'PASSWORD': 'super-secret-database', # @UndefinedVariable
        'HOST': 'pgsql.a.edu',
    }

}

