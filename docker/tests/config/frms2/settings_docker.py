# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

SERVER_REALM="frms2.edu"     # hs-mittweida.de
HTTP_SERVER_NAME="http://frms2-web" # https://saxid-api.hs-mittweida.de
ALLOWED_HOSTS = ["*"]   # saxid-api.hs-mittweida.de

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'frms2',
        'USER': 'frms2',
        'PASSWORD': 'frms2', # @UndefinedVariable
        'HOST': 'db2',
    }

}
