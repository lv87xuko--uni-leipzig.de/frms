#!/usr/bin/env bash
docker-compose up -d
sleep 20
python3.8 tests/test.py
success=$?
docker-compose down
exit $success
