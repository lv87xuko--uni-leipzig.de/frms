Deployment eines API Servers
============================

Systemvoraussetzungen:
* 1 VM mit Linux, per HTTPS von beteiligten Hochschulen erreichbar
* 1 CPU
* 2 GB RAM
* 20 GB Platte
Idealerweise bekommt die Maschine einen CNAME, damit bei Plattformupgrades der Name stabil bleiben kann

RedHat
------

```bash
yum install git python-virtualenv mod_wsgi
yum install postgresql-devel gcc 
```



Der Code vom Projekt ist unter
https://gitlab.hrz.tu-chemnitz.de/saxid-federated-resource-management/frms


```bash
for dir in env private static logs; do
  mkdir -p /var/www/django/frms/$dir
done
```

Datei /var/www/django/frms/private/settings_private.py mit folgendem Inhalt:

```python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

SECRET_KEY = "XYZ..." #60 Stellen Zufall für Django

DATABASE_PASSWORD = "XXXXX"
```
`SECRET_KEY` kann folgendermaßen generiert werden:
```python
import random, string
key = "".join([random.SystemRandom().choice(string.digits + string.letters + "!#$%&()*+,-./:;<=>?@[]^_`{|}~") for i in range(60)])
print("SECRET_KEY= '%s'" % key)
```

Datei /var/www/django/frms/private/settings_local.py mit ähnlichem Inhalt an, Parameter anpassen, im Beispiel für die Installation an der HS Mittweida:
```python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from settings_private import *
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'saxid',
        'USER': 'saxid_rw',
        'PASSWORD': DATABASE_PASSWORD,
        'HOST': 'postgresqql-host.hs-mittweida.de',
    },
}
# Der Realm vom EPPN der eigenen Nutzer
SERVER_REALM="hs-mittweida.de"
HTTP_SERVER_NAME="https://saxid-api.hs-mittweida.de"
ALLOWED_HOSTS = ["saxid-api.hs-mittweida.de"]
STATIC_URL  = '/static/api/'
AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.RemoteUserBackend',
]
```



```bash
cd /var/www/django/frms

git clone git@gitlab.hrz.tu-chemnitz.de:saxid-federated-resource-management/frms.git app

virtualenv /var/www/django/frms/env
. /var/www/django/frms/env/bin/activate
cd /var/www/django/frms/app
pip install -r requirements.txt
./manage.py migrate

# Hier als Nutzername das angeben, was per Apache als REMOTE_USER rauskommt
./manage.py createsuperuser
./manage.py collectstatic
```

Dann noch in der Apache config eintragen:

```
WSGIDaemonProcess frms display-name=%{GROUP} python-path=/var/www/django/frms/env/lib/python2.7/site-packages/:/var/www/django/frms/app/ lang='de_DE.UTF-8' locale='de_DE.UTF-8'
WSGIScriptAlias /api /var/www/django/frms/app/frms/wsgi.py process-group=frms
WSGIPassAuthorization On

<Directory /var/www/django/frms/app/frms>
    <Files wsgi.py>
        WSGIProcessGroup frms
        Require all granted
    </Files>
</Directory>

# staticfiles
Alias /static/api /var/www/django/frms/static/
<Directory /var/www/django/frms/static/>
    Require all granted
    RequestHeader unset Cookie
    # Static Content ist 2 Tage gültig
    Header set Cache-Control "max-age=172800, public, must-revalidate"
    Options -Indexes -FollowSymLinks
    AllowMethods GET HEAD OPTIONS
    AllowOverride None
</Directory>


<Location "/api">

    <RequireAll>
        <RequireAny>
            Require ip 134.109.0.0/16
            Require ip 2001:638:911::/48
        </RequireAny>
    </RequireAll>
</Location>
<Location "/api/admin">
    AuthType shibboleth
    ShibRequireSession On
    Require user schrd
    Require user klada
</Location>
```

Cronjobs und Dienste
====================

Es muß der Sync Dienst laufen:

```bash
    cp sync.service /etc/systemd/system/
    systemctl enable sync
    systemctl start sync
```

Wenn Daten per CSV importiert werden sollen, dann geht das per
```bash
/var/www/django/frms/env/bin/python2 /var/www/django/frms/app/manage.py sync_from_csv export.csv
```

Monitoring
==========
Im Monitoring kann der Zustand folgendermaßen überwacht werden:
```bash
/var/www/django/frms/env/bin/python2 /var/www/django/frms/app/manage.py check_health -v2
```
Return Code 0 heißt alles OK, Ungleich 0 heißt Fehler

Update
======
Wenn ein update ansteht, dann wäre folgendes zu tun:
```bash
. /var/www/django/frms/env/bin/activate
cd /var/www/django/frms/app
git pull
pip install -r requirements.txt
./manage.py collectstatic
touch frms/wsgi.py
```

Wenn Sie /api/admin nicht per Shibboleth schützen wollen, können Sie auch die Django Authentifizierung nutzen. Da sind dann ein paar kleine Änderungen in settings_local nötig.

Wenn alles funktioniert hat, müßten Sie unter `https://<fqdn>/api/res` eine Seite vom Django Rest Framework sehen.


Partner einrichten
==================

wenn das Django läuft, wäre noch folgendes zu tun:

Gehen Sie in den Django Admin und legen dort einen neuen Benutzer für die Synchronisation mit der Partnereinrichtung an mit folgenden Parametern:

Passwort ist egal, spielt eh keine Rolle, Auth geht per Token.
Benutzername: ist im Prinzip aber egal. In Chemnitz nutzen wir den FQDN der Partnerhochschule. Authentifizierung geht per Token
User type: sync user
REALM: den REALM vom EPPN der Partnerhochschule, z.B. tu-chemnitz.de
Api url: URL, unter der die API der Partnerhochschule erreichbar ist, z.B. https://saxid.hrz.tu-chemnitz.de/api/

Einer von beiden beteiligten Partnern muß das Token wechseln, beide benötigen dasselbe Token.
Dann noch das Token wechseln oder mitschickst mir das Token, das generiert wurde und ich ändere es. Es muß nur dasselbe Token sein.
```bash
./manage.py change_token
```

Bekannte API Instanzen in Sachsen:

* TU Chemnitz: https://saxid.hrz.tu-chemnitz.de/api/
* HS Mittweida: https://saxid-api.hs-mittweida.de/api/
* TU Freiberg: https://saxid.hrz.tu-freiberg.de/api/

Service Provider hinzufügen
===========================

siehe [separate Anleitung](new-service-provider.md)
