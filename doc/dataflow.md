Datenfluss in FRMS (SaxID API)
==============================

Begriffe
--------

* Service Provider - Das System, das den Dienst bereit stellt, den der Nutzer 
  nutzen möchte, z.B. GitLab, Owncloud, HPC, u.a. Der Dienst implementiert
  üblicherweise einen SAML 1/2 Service Provider, z.B. mit Shibboleth. Über SAML
  werden Nutzer authentifiziert. Der Service Provider befindet sich bei der Dienst
  erbringenden Einrichtugn.
* Identity Provider - Das System, das den Nutzer authentifiziert. Ist üblicherweise 
  ein SAML 1/2 Identity Provider, z.B. Shibboleth. Der Identity Provider befindet 
  sich in der Heimateinrichtung des Nutzers.
* Identity Management System - Das System, das Nutzerkonten und -daten speichert und 
  vorhält. Der Identity Provider nutzt diese Daten, um zwischen gültigen und 
  ungültigen Nutzern zu unterscheiden. Außerdem stellt das Identity Management System 
  dem Identity Provider zusätzliche Attribute wie zum Beispiel den Namen des Nutzers 
  zur Verfügung.
* FRMS (SaxID API) - System, das Daten zwischen Diensterbringenden und
  Dienstnutzenden Einrichtung vermittelt
* Ressource - Datensatz in FRMS, der das zu verwaltende Objekt beim Service
  Provider repräsentiert. Dies kann beispielsweise ein Benutzeraccount beim
  Service provider sein.
* EPPN (EduPersonPricipalName)

Einsatzzweck des Systems
------------------------

Bei der verteilten Authentifizierung über SAML findet eine Datenübertragung
zwischen Identity Provider und Service Provider nur dann statt, wenn der
Benutzer die Anwendung des Service Providers nutzt. Für den Service Provider
besteht die Möglichkeit, einzelne Attribute des Nutzers später per SAML 2
Attribute Query abzufragen. Eine Möglichkeit in umgekehrter Richtung Daten 
an den Identity Provider zu übermitteln besteht nicht.

In verteilten Umgebungen mit großen Nutzerzahlen stellt dies ein Problem dar,
weil folgende Anwendungsszenarien nicht abgedeckt werden können:

* Der Service Provider kann keine Ressourcen von Nutzern freigeben, deren
  Benutzungsberechtigung abgelaufen ist, da er das Ablaufdatum nicht kennt.
* Der Service Provider kann der Heimateinrichtung keine Daten über die Nutzung
  des Dienstes bereit stellen. Bei Speicher- und damit Kostenintensiven
  Diensten wie Speicher für Sync and Share Dienste kann dadurch keine
  Kapazitätsplanung durch die Heimateinrichtung stattfinden.
* Die Parameter des Dienstes des Service Providers können nicht in das Service
  Management Portal der Heimateinrichtung integriert werden. Darüber kann zum
  Beispiel eine Möglichkeit geschaffen werden, dass Nutzer über das Service
  Management Portal das zur Verfügung gestellte Speicherkontingent im
  vorgegebenen Rahmen selbst verwalten können.
* Die Heimateinrichtung hat keine Möglichkeit, über ein Push-Verfahren Nutzer
  ihrer Einrichtung bei einem Service Provider unverzüglich zu sperren.

Diese Szenarien können mit FRMS abgebildet werden.

Datenfluss
----------

![data flow diagram](dataflow.png)

Bei erstmaliger Nutzung des Service Providers legt dieser die Ressource im
Dienst an und fügt der FRMS API einen Ressourcendatensatz hinzu (1). Dabei wird
mindestens der EPPN des Nutzers übertragen. Der EPPN wurde dem Service Provider
über SAML vom Identity Provider bereit gestellti (0). Zusätzlich zum EPPN wird das
expiryDate und deletionDate Attribut übertragen. Da der Service Provider die
wirklichen Daten nicht kennt, wählt er dafür einen geeigneten Zeitpunkt in der
Zukunft, beispielsweise aktuelle Zeit + 1 Woche. Über die beiden Attribute kann
die Heimateinrichtung des Nutzers steuern, wann die Ressource beim Dienst
gesperrt (expiryDate) bzw. gelöscht (deletionDate) werden soll. Abhängig vom
Dienst können weitere Attribute übertragen werden. 

Die FRMS Instanz des Service Providers repliziert den Ressourcendatensatz zur
FRMS Instanz der Heimateinrichtung (2). Die Heimateinrichtung kann dem EPPN
entnommen werden. Die Heimateinrichtung gleicht die Daten der Ressource mit
ihrem Identity Management System und gegebenefalls weiteren Systemen ab und
aktualisiert diese auf geeignete Weise (3). Die FRMS Instanz der Heimateinrichtung
wird die Änderung wieder zur FRMS Instanz des Service Providers replizieren (4).

Zeitgleiche Änderungen desselben Attributs bei Service Provider und Identity
Provider werden über Versionsvektoren erkannt. Im Konfliktfall gibt es Regeln,
die den Vorrang für die Attribute bestimmen:

* bei deletionDate und expiryDate hat das Attribut der Heimateinrichtung Vorrang
* bei allen anderen Attributen hat das Attribut des Service Providers Vorrang

Die Daten der FRMS Instanz des Service Providers werden mit dem Service Provider 
abgeglichen (5).

Sperren von Ressourcen
----------------------

Die Sperrung von Ressourcen unterliegt komplett der Steuerung durch die
Heimateinrichtung. Die Steuerung erfolgt über das expiryDate Attribut. Sobald
der im expiryDate angegebene Zeitpunkt erreicht ist, ist der Service Provider
verpflichtet, die Nutzung der Ressource zu sperren.

Die Heimateinrichtung des Nutzers muß sicherstellen, dass der im expiryDate
Attribut angegeben Zeitwert solange in der Zukunft liegt, wie der Nutzer zur
Nutzung berechtigt ist. Es ist nicht notwendig, dass die Heimateinrichtung das
wahre Ablaufdatum des Benutzeraccounts als expiryDate übermittelt.

Löschen von Ressourcen
----------------------

Die Löschung von Ressourcen unterliegt komplett der Steuerung durch die
Heimateinrichtung. Die Steuerung erfolgt über das deletionDate Attribut. Sobald
der aktuelle Zeitpunkt größer als der im deletionDate angegebene Zeitpunkt ist,
ist der Service Provider verpflichtet, die Löschung der Ressource zu
veranlassen. Das deletionDate muß größer als oder gleich dem expiryDate sein.

Durch diese Architektur obliegt es der Heimateinrichtung, die Löschung von
Ressourcen zu veranalassen und die Verantwortung dafür zu tragen. Jede
Heimateinrichtung kann für den gleichen Service Provider unterschiedliche
Löschpolicies festlegen. Jede Heimateinrichtung kann für unterschiedliche
Service Provider unterschiedliche Löschpolicies festlegen.
