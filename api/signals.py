# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

from django.dispatch import Signal

resource_created = Signal(providing_args=["resource", "user_type"])

resource_updated = Signal(providing_args=["resource", "user_type"])

