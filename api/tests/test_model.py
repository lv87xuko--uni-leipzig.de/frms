# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.authtoken.models import Token
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from api.models import TokenAuthUser, UserType


class TokenAuthUserTest(APITestCase):
    def setUp(self):
        self.a_super = TokenAuthUser.objects.create(username="a_super",
                                                    email="test@a.edu",
                                                    password="a_super",
                                                    user_type=UserType.TYPE_SYNC,
                                                    api_url="http://api.a.edu:7000/foobar/",
                                                    realm="a.edu")
    def test_remote_reverse(self):
        self.assertEqual(
            self.a_super.remote_reverse("api:health"),
            "http://api.a.edu:7000/foobar/health/"
        )
