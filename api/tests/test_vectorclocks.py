# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from unittest import TestCase

from api.utils.vector_clock import VectorClock


class VectorClockTest(TestCase):
    def setUp(self):
        self.a = VectorClock(0, 0)
        self.b = VectorClock(0, 1)
        self.c = VectorClock(1, 0)
        self.d = VectorClock(1, 1)

    def test_happened_before(self):
        self.assertTrue(self.a.happened_before(self.b))
        self.assertTrue(self.a.happened_before(self.c))
        self.assertTrue(self.a.happened_before(self.d))

        self.assertFalse(self.b.happened_before(self.a))
        self.assertFalse(self.b.happened_before(self.c))
        self.assertFalse(self.c.happened_before(self.b))
        self.assertFalse(self.d.happened_before(self.a))

    def test_concurrent_to(self):
        self.assertTrue(self.b.concurrent_to(self.c))
        self.assertTrue(self.c.concurrent_to(self.b))

        self.assertFalse(self.a.concurrent_to(self.b))
        self.assertFalse(self.b.concurrent_to(self.a))

    def test_merge(self):
        temp = self.a
        self.assertEqual(temp.merge(self.b), (0, 1))

        temp = self.b
        self.assertEqual(temp.merge(self.c), (1, 1))

        temp = self.d
        self.assertEqual(temp.merge(self.a), (1, 1))
