#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals

import requests_mock
from django.test import TestCase
from api.models import Attribute, QueueJob, QueueJobStatus, QueueJobType, Resource, ServiceProvider, TokenAuthUser, UserType
from api.utils import queue_utils

import json
import six


class DispatchRessourceSyncTest(TestCase):
    def test_resource(self):
        self.assertEqual(QueueJob.objects.count(), 0)

        remote_realm = 'foo.bar'
        remote_user = TokenAuthUser(user_type=UserType.TYPE_SYNC, username='foobar', realm=remote_realm, api_url='https://remote.example.org/api')
        remote_user.set_unusable_password()
        remote_user.full_clean()
        remote_user.save()
        sp = ServiceProvider(realm=remote_realm, name="foo2", expiry_date='2099-01-01 00:00:00Z')
        sp.full_clean()
        sp.save()
        r = Resource(sp=sp, eppn='user@foo.bar', expiry_date='2016-01-01 00:00:00+01:00', deletion_date='2017-01-01 00:00:00+01:00', setup_token='1234')
        r.full_clean()
        r.save()
        att = Attribute(resource=r, name="foo", value="1234")
        att.full_clean()
        att.save()
        att2 = Attribute(resource=r, name="bar", value="5678")
        att2.full_clean()
        att2.save()

        queue_utils.dispatch_resource_sync(r)
        self.assertEqual(QueueJob.objects.count(), 1)
        qj = QueueJob.objects.first()
        self.assertEqual(qj.status, QueueJobStatus.STATUS_READY)
        self.assertEqual(qj.type, QueueJobType.TYPE_RES)
        object_data = json.loads(qj.obj)
        self.assertEqual(object_data['uuid'], six.text_type(r.pk))
        self.assertEqual(object_data['realm_idp'], r.realm_idp)
        self.assertEqual(object_data['eppn'], r.eppn)
        self.assertEqual(object_data['sp'], six.text_type(r.sp.pk))
        self.assertEqual(len(object_data['attributes']), 2)
        for a in (att, att2):
            self.assertTrue({'default_value': '', 'name': a.name, 'value': a.value, 'vec_idp': a.vec_idp, 'vec_sp': a.vec_sp} in object_data['attributes'])


class SyncResourceTest(TestCase):
    def setUp(self):
        self.remote_url = 'https://remote.example.org/api'
        self.remote_realm = 'foo.bar'
        self.remote_user = TokenAuthUser(user_type=UserType.TYPE_SYNC, username='foobar', realm=self.remote_realm, api_url=self.remote_url)
        self.remote_user.set_unusable_password()
        self.remote_user.full_clean()
        self.remote_user.save()

    def test_success(self):
        """
        Dieser Test testet, ob die Daten remote gepostet werden
        """
        transfer_data = '{"foo": "bar"}'
        with requests_mock.Mocker() as m:
            m.register_uri('POST', self.remote_user.remote_reverse("api:resource_list"), status_code=201)
            queue_utils.sync_resource(self.remote_user, transfer_data)
        self.assertTrue(m.called)
        # content matches
        self.assertEquals(m.last_request.text, transfer_data)
        # authenticated
        self.assertEquals(m.last_request.headers['Authorization'], "token {}".format(self.remote_user.token()))

    def test_error(self):
        """
        Dieser Test testet, ob die Daten remote gepostet werden
        """
        transfer_data = '{"foo": "bar"}'
        with requests_mock.Mocker() as m:
            m.register_uri('POST', self.remote_user.remote_reverse("api:resource_list"), status_code=500)
            with self.assertRaises(queue_utils.SyncException):
                queue_utils.sync_resource(self.remote_user, transfer_data)
        self.assertTrue(m.called)

    def test_double_post(self):
        transfer_data = '{"foo": "bar", "uuid": "12345"}'
        uuid = '12345'
        with requests_mock.Mocker() as m:
            m.register_uri('POST', self.remote_user.remote_reverse("api:resource_list"), status_code=400)
            m.register_uri('GET', self.remote_user.remote_reverse("api:resource_detail", args=(uuid, )), status_code=200)
            queue_utils.sync_resource(self.remote_user, transfer_data)
        self.assertTrue(m.called)

    def test_double_post_failed(self):
        transfer_data = '{"foo": "bar", "uuid": "12345"}'
        uuid = '12345'
        with requests_mock.Mocker() as m:
            m.register_uri('POST', self.remote_user.remote_reverse("api:resource_list"), status_code=400)
            m.register_uri('GET', self.remote_user.remote_reverse("api:resource_detail", args=(uuid, )), status_code=404)
            with self.assertRaises(queue_utils.SyncException):
                queue_utils.sync_resource(self.remote_user, transfer_data)
        self.assertTrue(m.called)


class SyncAttributesTest(TestCase):
    def setUp(self):
        self.remote_url = 'https://remote.example.org/api'
        self.remote_realm = 'foo.bar'
        self.remote_user = TokenAuthUser(user_type=UserType.TYPE_SYNC, username='foobar', realm=self.remote_realm, api_url=self.remote_url)
        self.remote_user.set_unusable_password()
        self.remote_user.full_clean()
        self.remote_user.save()
        sp = ServiceProvider(realm=self.remote_realm, name="foo2", expiry_date='2099-01-01 00:00:00Z')
        sp.full_clean()
        sp.save()
        self.r = Resource(sp=sp, eppn='user@foo.bar', expiry_date='2016-01-01 00:00:00+01:00', deletion_date='2017-01-01 00:00:00+01:00', setup_token='1234')
        self.r.full_clean()
        self.r.save()
        self.foo = Attribute(resource=self.r, name="foo", value="1234")
        self.foo.full_clean()
        self.foo.save()
        self.bar = Attribute(resource=self.r, name="bar", value="5678")
        self.bar.full_clean()
        self.bar.save()

    def test_success(self):
        transfer_data = json.dumps({"foo": "bar", "uuid": str(self.r.uuid)})
        answer = json.dumps([
            {
                'foo': {'vec_idp': 2, 'vec_sp': 3, 'status': 'updated'},
            }
        ])
        with requests_mock.Mocker() as m:
            m.register_uri('PUT', self.remote_user.remote_reverse("api:resource_attributes", args=(self.r.pk, )), status_code=200, text=answer)
            queue_utils.sync_attributes(self.remote_user, transfer_data, str(self.r.pk))
        foo = Attribute.objects.get(pk=self.foo.pk)
        self.assertEquals(foo.vec_idp, 2)
        self.assertEquals(foo.vec_sp, 3)

    def test_failure(self):
        transfer_data = json.dumps({"foo": "bar", "uuid": str(self.r.uuid)})
        answer = json.dumps([
            {
                'foo': {'vec_idp': 2, 'vec_sp': 3, 'status': 'updated'},
            }
        ])
        with requests_mock.Mocker() as m:
            m.register_uri('PUT', self.remote_user.remote_reverse("api:resource_attributes", args=(self.r.pk, )), status_code=404, text=answer)
            with self.assertRaises(queue_utils.SyncException):
                queue_utils.sync_attributes(self.remote_user, transfer_data, str(self.r.pk))


class DeleteResourceTest(TestCase):
    def setUp(self):
        self.remote_url = 'https://remote.example.org/api'
        self.remote_realm = 'foo.bar'
        self.remote_user = TokenAuthUser(user_type=UserType.TYPE_SYNC, username='foobar', realm=self.remote_realm, api_url=self.remote_url)
        self.remote_user.set_unusable_password()
        self.remote_user.full_clean()
        self.remote_user.save()

    def test_success(self):
        uuid = "12345"
        with requests_mock.Mocker() as m:
            m.register_uri('DELETE', self.remote_user.remote_reverse("api:resource_detail", args=(uuid, )), status_code=200)
            queue_utils.delete_resource(self.remote_user, uuid)

    def test_not_found(self):
        uuid = "12345"
        with requests_mock.Mocker() as m:
            m.register_uri('DELETE', self.remote_user.remote_reverse("api:resource_detail", args=(uuid, )), status_code=404)
            queue_utils.delete_resource(self.remote_user, uuid)

    def test_failure(self):
        uuid = "12345"
        with requests_mock.Mocker() as m:
            m.register_uri('DELETE', self.remote_user.remote_reverse("api:resource_detail", args=(uuid, )), status_code=500)
            with self.assertRaises(queue_utils.SyncException):
                queue_utils.delete_resource(self.remote_user, uuid)
