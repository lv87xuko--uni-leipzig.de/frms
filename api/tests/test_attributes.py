# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils import timezone
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from api.models import TokenAuthUser, UserType, Resource, ServiceProvider, Attribute, QueueJob


class AttributeUpdateTest(APITestCase):
    def setUp(self):
        TokenAuthUser.objects.create_superuser(username="a_super",
                                               email="test@a.edu",
                                               password="a_super",
                                               user_type=UserType.TYPE_API,
                                               api_url="http://api.a.edu:7000/",
                                               realm="a.edu")

        TokenAuthUser.objects.create_superuser(username="b_sync",
                                               email="test@b.edu",
                                               password="b_sync",
                                               user_type=UserType.TYPE_SYNC,
                                               api_url="http://api.b.edu:7001/",
                                               realm="b.edu")

        sp = ServiceProvider.objects.create(name="TestService",
                                            expiry_date=timezone.now(),
                                            realm="a.edu")

        self.res = Resource.objects.create(setup_token="Lorem ipsum",
                                           eppn="u1@b.edu",
                                           expiry_date=timezone.now(),
                                           deletion_date=timezone.now(),
                                           sp=sp)

        self.attrib = Attribute.objects.create(name="Quota",
                                               default_value="100",
                                               value=50,
                                               resource=self.res,
                                               vec_sp=1,
                                               vec_idp=0)

        TokenAuthUser.objects.create_superuser(username="testservice",
                                               email="testservice@a.edu",
                                               password="a_super",
                                               user_type=UserType.TYPE_SP,
                                               api_url="http://api.a.edu:7000/",
                                               realm="a.edu",
                                               sp=sp,
                                              )

    def test_attribute_update(self):
        url = reverse('api:resource_attributes', args=[self.res.uuid])

        user = TokenAuthUser.objects.get(username='a_super')
        self.client.force_authenticate(user=user, token=user.token())

        data = [
            {
                "name": "Quota",
                "value": 23
            },
            {
                "name": "cpuTime",
                "value": 42
            }
        ]

        self.assertEqual(0, QueueJob.objects.count())
        response = self.client.put(url, data=data, format='json')
        print(response.data)

        self.assertEqual("updated", response.data[0]["Quota"]["status"])
        self.assertEqual(50, self.attrib.value)

        self.assertEqual("non-existent", response.data[1]["cpuTime"]["status"])
        self.assertEqual(1, QueueJob.objects.count())

        data = [
            {
                "name": "Quota",
                "value": 23
            }
        ]

        response = self.client.put(url, data=data, format='json')

        self.assertEqual("updated", response.data[0]["Quota"]["status"])
        self.assertEqual(23, int(Attribute.objects.get().value))
        self.assertEqual(3, Attribute.objects.get().vec_sp)
        self.assertEqual(0, Attribute.objects.get().vec_idp)
        self.assertEqual(2, QueueJob.objects.count())

    def test_attribute_update_as_sp(self):
        url = reverse('api:resource_attributes', args=[self.res.uuid])

        user = TokenAuthUser.objects.get(username='testservice')
        self.client.force_authenticate(user=user, token=user.token())

        data = [
            {
                "name": "Quota",
                "value": 23
            },
            {
                "name": "cpuTime",
                "value": 42
            }
        ]

        self.assertEqual(0, QueueJob.objects.count())
        response = self.client.put(url, data=data, format='json')

        self.assertEqual("updated", response.data[0]["Quota"]["status"])
        self.assertEqual(50, self.attrib.value)

        self.assertEqual("non-existent", response.data[1]["cpuTime"]["status"])
        self.assertEqual(1, QueueJob.objects.count())

        data = [
            {
                "name": "Quota",
                "value": 23
            }
        ]

        response = self.client.put(url, data=data, format='json')

        self.assertEqual("updated", response.data[0]["Quota"]["status"])
        self.assertEqual(23, int(Attribute.objects.get().value))
        self.assertEqual(3, Attribute.objects.get().vec_sp)
        self.assertEqual(0, Attribute.objects.get().vec_idp)
        self.assertEqual(2, QueueJob.objects.count())

    def test_attribute_update_rejected(self):
        url = reverse('api:resource_attributes', args=[self.res.uuid])

        user = TokenAuthUser.objects.get(username='b_sync')
        self.client.force_authenticate(user=user, token=user.token())

        data = [
            {
                "name": "Quota",
                "value": 23,
                "vec_idp": 3,
                "vec_sp": 0,
            },
        ]

        self.assertEqual(0, QueueJob.objects.count())
        response = self.client.put(url, data=data, format='json')

        self.assertEqual("rejected", response.data[0]["Quota"]["status"])
        self.assertEqual(50, self.attrib.value)
