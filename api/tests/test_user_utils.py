#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals
# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

from django.conf import settings
from django.test import override_settings, TestCase

from api.models import Resource, ServiceProvider, TokenAuthUser, UserType
from api.utils import user_utils


class UserUtilTest(TestCase):
    def test_get_local_token(self):
        u = TokenAuthUser(user_type=UserType.TYPE_API, username='self')
        u.set_unusable_password()
        u.full_clean()
        u.save()
        token = user_utils.get_local_token()
        self.assertEqual(len(token), 40)
        self.assertEqual(token, u.token())

    def test_get_remote_url_by_realm_without_user(self):
        self.assertEqual(user_utils.get_remote_url_by_realm('foobar'), None)

    def test_get_remote_url_by_realm_with_user(self):
        u = TokenAuthUser(user_type=UserType.TYPE_SYNC, username='foobar', realm='foobar', api_url='https://saxid.example.org/api')
        u.set_unusable_password()
        u.full_clean()
        u.save()
        self.assertEqual(user_utils.get_remote_url_by_realm('foobar'), 'https://saxid.example.org/api')

    def test_get_remote_token_by_realm_without_user(self):
        self.assertEqual(user_utils.get_remote_token_by_realm('foobar'), '')

    def test_get_remote_token_by_realm_with_user(self):
        u = TokenAuthUser(user_type=UserType.TYPE_SYNC, username='foobar', realm='foobar', api_url='https://saxid.example.org/api')
        u.set_unusable_password()
        u.full_clean()
        u.save()
        self.assertEqual(user_utils.get_remote_token_by_realm('foobar'), u.token())

    @override_settings(
        SERVER_REALM="local.realm"
    )
    def test_get_remote_url_by_res_uuid(self):
        my_user = TokenAuthUser(user_type=UserType.TYPE_API, username='self', realm=settings.SERVER_REALM, api_url='https://local.example.org/api')
        my_user.set_unusable_password()
        my_user.full_clean()
        my_user.save()
        remote_realm = 'foo.bar'
        remote_user = TokenAuthUser(user_type=UserType.TYPE_SYNC, username='foobar', realm=remote_realm, api_url='https://remote.example.org/api')
        remote_user.set_unusable_password()
        remote_user.full_clean()
        remote_user.save()

        # this case: we are service provider, user is from different instance
        sp = ServiceProvider(realm=settings.SERVER_REALM, name="foo", expiry_date='2099-01-01 00:00:00Z')
        sp.full_clean()
        sp.save()
        r = Resource(sp=sp, eppn="user@{}".format(remote_realm), expiry_date='2016-12-16 00:00:00+01:00', deletion_date='2016-12-16 00:00:00+01:00')
        r.save()
        self.assertEqual(user_utils.get_remote_url_by_res_uuid(r.pk), remote_user.api_url)
        
        # next case: we are identity provider, resource is on different instance
        sp2 = ServiceProvider(realm=remote_realm, name="foo2", expiry_date='2099-01-01')
        sp2.full_clean()
        sp2.save()
        r = Resource(sp=sp2, eppn="user@{}".format(settings.SERVER_REALM), expiry_date='2016-12-16 00:00:00+01:00', deletion_date='2016-12-16 00:00:00+01:00')
        r.save()
        self.assertEqual(user_utils.get_remote_url_by_res_uuid(r.pk), remote_user.api_url)
    @override_settings(
        SERVER_REALM="local.realm"
    )
    def test_get_remote_token_by_res_uuid(self):
        my_user = TokenAuthUser(user_type=UserType.TYPE_API, username='self', realm=settings.SERVER_REALM, api_url='https://local.example.org/api')
        my_user.set_unusable_password()
        my_user.full_clean()
        my_user.save()
        remote_realm = 'foo.bar'
        remote_user = TokenAuthUser(user_type=UserType.TYPE_SYNC, username='foobar', realm=remote_realm, api_url='https://remote.example.org/api')
        remote_user.set_unusable_password()
        remote_user.full_clean()
        remote_user.save()

        # this case: we are service provider, user is from different instance
        sp = ServiceProvider(realm=settings.SERVER_REALM, name="foo", expiry_date='2099-01-01 00:00:00Z')
        sp.full_clean()
        sp.save()
        r = Resource(sp=sp, eppn="user@{}".format(remote_realm), expiry_date='2016-12-16 00:00:00Z', deletion_date='2016-12-16 00:00:00Z')
        r.save()
        self.assertEqual(user_utils.get_remote_token_by_res_uuid(r.pk), remote_user.token())
        
        # next case: we are identity provider, resource is on different instance
        sp2 = ServiceProvider(realm=remote_realm, name="foo2", expiry_date='2099-01-01')
        sp2.full_clean()
        sp2.save()
        r = Resource(sp=sp2, eppn="user@{}".format(settings.SERVER_REALM), expiry_date='2016-12-16 00:00:00+01:00', deletion_date='2016-12-16 00:00:00+01:00')
        r.save()
        self.assertEqual(user_utils.get_remote_token_by_res_uuid(r.pk), remote_user.token())
