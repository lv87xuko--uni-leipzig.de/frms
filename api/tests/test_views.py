# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

from datetime import datetime
from datetime import timedelta
import uuid
from unittest import skip

from django.core.files.uploadedfile import SimpleUploadedFile
from django.conf import settings
from django.test import override_settings
from django.utils import timezone
from django.test.client import encode_multipart
from rest_framework.renderers import JSONRenderer
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase
from api.models import Attribute, QueueJob, Resource, ServiceProvider, TokenAuthUser, UserType
from api.serializers import ResourceSerializer


@override_settings(SERVER_REALM="a.edu")
class ResourceDeleteTest(APITestCase):
    def setUp(self):
        tomorrow = timezone.now() + timezone.timedelta(days=1)
        self.idp_user = TokenAuthUser.objects.create_superuser(
            username='idp',
            email='foo@bar.com',
            password='idp',
            user_type=UserType.TYPE_IDP,
            api_url="http://api.a.edu:7000/",
            realm="a.edu",
        )
        self.sync_user = TokenAuthUser.objects.create_superuser(
            username='sync',
            email='foo@bar.com',
            password='sync',
            user_type=UserType.TYPE_SYNC,
            api_url="http://api.b.edu:7000/",
            realm="b.edu",
        )
        self.sp_user = TokenAuthUser.objects.create_superuser(
            username='sp',
            email='foo@bar.com',
            password='sp',
            user_type=UserType.TYPE_SP,
            api_url="http://api.a.edu:7000/",
            realm="a.edu",
        )

        self.local_sp = ServiceProvider.objects.create(
            name="TestService",
            expiry_date=timezone.now(),
            realm="a.edu"
        )
        self.remote_sp = ServiceProvider.objects.create(
            name="RemoteService",
            expiry_date=timezone.now(),
            realm="b.edu",
        )

        self.localResource = Resource.objects.create(
            eppn="usera@a.edu",
            setup_token="1234",
            expiry_date=tomorrow,
            deletion_date=tomorrow,
            sp=self.local_sp,
        )
        self.remoteResource = Resource.objects.create(
            eppn="usera@a.edu",
            setup_token="1234",
            expiry_date=tomorrow,
            deletion_date=tomorrow,
            sp=self.remote_sp,
        )
        self.remoteUserLocalResource = Resource.objects.create(
            eppn="usera@b.edu",
            setup_token="1234",
            expiry_date=tomorrow,
            deletion_date=tomorrow,
            sp=self.local_sp,
        )

    def _test_delete(self, res, user, queue_delta):
        qlen = QueueJob.objects.filter(res_uuid=res.uuid).count()
        self.client.force_authenticate(user=user, token=user.token())
        self.client.delete(reverse('api:resource_detail', kwargs={'uuid': res.uuid}))
        # resource has been deleted
        self.assertEqual(Resource.objects.filter(uuid=res.uuid).count(), 0)
        # no queueJob has been created
        self.assertEqual(QueueJob.objects.filter(res_uuid=res.uuid).count(), qlen + queue_delta)

    def test_idp_delete_local_resource(self):
        self._test_delete(self.localResource, self.idp_user, 0)

    def test_idp_delete_remote_resource(self):
        self._test_delete(self.remoteResource, self.idp_user, 1)

    def test_sync_delete_local_resource(self):
        self._test_delete(self.localResource, self.sync_user, 0)

    def test_sync_delete_remote_resource(self):
        self._test_delete(self.localResource, self.sync_user, 0)

    def test_sp_delete_local_resource(self):
        self._test_delete(self.localResource, self.sp_user, 0)

    def test_sp_delete_remote_resource(self):
        self._test_delete(self.remoteUserLocalResource, self.sp_user, 1)


class ResourceCreateTest(APITestCase):

    def setUp(self):
        tomorrow = timezone.now() + timezone.timedelta(days=1)
        self.idp_user = TokenAuthUser.objects.create_superuser(
            username='idp',
            email='foo@bar.com',
            password='idp',
            user_type=UserType.TYPE_IDP,
            api_url="http://api.a.edu:7000/",
            realm="a.edu",
        )
        self.sync_user = TokenAuthUser.objects.create_superuser(
            username='sync',
            email='foo@bar.com',
            password='sync',
            user_type=UserType.TYPE_SYNC,
            api_url="http://api.b.edu:7000/",
            realm="b.edu",
        )
        self.local_sp = ServiceProvider.objects.create(
            name="TestService",
            expiry_date=timezone.now(),
            realm="a.edu"
        )
        self.sp_user = TokenAuthUser.objects.create_superuser(
            username='sp',
            email='foo@bar.com',
            password='sp',
            user_type=UserType.TYPE_SP,
            api_url="http://api.a.edu:7000/",
            realm="a.edu",
            sp=self.local_sp,
        )

        self.localResource = Resource(
            eppn="usera@a.edu",
            setup_token="1234",
            expiry_date=tomorrow,
            deletion_date=tomorrow,
            sp=self.local_sp,
            sp_primary_key="usera@a.edu",
        )
        self.url = reverse('api:resource_list')
        self.res = Resource()
        self.queue_data = JSONRenderer().render(ResourceSerializer(self.localResource).data)

    def _test_as_user(self, user, expected_status_code):
        self.client.force_authenticate(user=user, token=user.token())
        r = self.client.post(self.url, data=self.queue_data, content_type="application/json")
        self.assertEqual(r.status_code, expected_status_code)

    def _test_create_from_sp(self, user, expected_status_code):
        self.client.force_authenticate(user=user, token=user.token())
        data = ResourceSerializer(self.localResource).data
        data.pop("uuid")
        data = JSONRenderer().render(data)
        r = self.client.post(self.url, data=data, content_type="application/json")
        self.assertEqual(r.status_code, expected_status_code)

    @skip("This known to be broken, Resources should be created by SP or sync users only")
    def test_create_as_idp_user(self):
        self._test_as_user(self.idp_user, 403)

    def test_create_as_sync_user(self):
        self._test_as_user(self.sync_user, 201)

    def test_create_as_sp_user(self):
        self._test_as_user(self.sp_user, 201)
        self.assertEqual(Resource.objects.filter(eppn=self.localResource.eppn, sp=self.local_sp).count(), 1)

    def test_double_create_as_sp(self):
        self._test_create_from_sp(self.sp_user, 201)
        self._test_create_from_sp(self.sp_user, 201)
        self.assertEqual(Resource.objects.filter(eppn=self.localResource.eppn, sp=self.local_sp).count(), 1)


class ResourceGetTest(APITestCase):
    def setUp(self):
        self.local_sp = ServiceProvider.objects.create(
            name="TestService",
            expiry_date=timezone.now(),
            realm="a.edu"
        )
        self.sp_user = TokenAuthUser.objects.create_superuser(
            username='sp',
            email='foo@bar.com',
            password='sp',
            user_type=UserType.TYPE_SP,
            api_url="http://api.a.edu:7000/",
            realm="a.edu",
            sp=self.local_sp,
        )

    def test_unauthenticated(self):
        invalid_uuid = str(uuid.uuid4())
        url = reverse("api:resource_detail", args=(invalid_uuid,))
        r = self.client.get(url)
        self.assertEqual(r.status_code, 401)

    def test_not_exist(self):
        invalid_uuid = str(uuid.uuid4())
        self.client.force_authenticate(self.sp_user)
        url = reverse("api:resource_detail", args=(invalid_uuid,))
        r = self.client.get(url)
        self.assertEqual(r.status_code, 404)

    def test_regular(self):
        r = Resource(sp=self.local_sp, expiry_date=timezone.now(), deletion_date=timezone.now(), setup_token="1234", eppn="foo@foo.bar")
        r.full_clean()
        r.save()
        self.client.force_authenticate(self.sp_user)
        url = reverse("api:resource_detail", args=(r.pk,))
        r = self.client.get(url)
        self.assertEqual(r.status_code, 200)


class ResourceUpdateTest(APITestCase):
    def setUp(self):
        self.sp = ServiceProvider.objects.create(
            name="TestService",
            expiry_date=timezone.now(),
            realm="a.edu"
        )
        self.sp_user = TokenAuthUser.objects.create_superuser(
            username='sp',
            email='foo@bar.com',
            password='sp',
            user_type=UserType.TYPE_SP,
            api_url="http://api.a.edu:7000/",
            realm="a.edu",
            sp=self.sp,
        )
        self.idp_user = TokenAuthUser.objects.create_superuser(
            username='idp',
            email='foo@bar.com',
            password='idp',
            user_type=UserType.TYPE_IDP,
            api_url="http://api.b.edu:7000/",
            realm="b.edu",
        )
        self.sync_user = TokenAuthUser.objects.create_superuser(
            username='sync',
            email='foo@bar.com',
            password='idp',
            user_type=UserType.TYPE_SYNC,
            api_url="http://api.b.edu:7000/",
            realm="b.edu",
        )
        self.r = Resource(sp=self.sp, expiry_date=timezone.now(), deletion_date=timezone.now(), setup_token="1234", eppn="foo@b.edu")
        self.r.full_clean()
        self.r.save()
        self.attr1 = Attribute(name='test', value='foo', vec_sp=3, vec_idp=1, resource=self.r)
        self.attr1.full_clean()
        self.attr1.save()
        self.attr2 = Attribute(name='other', value='bar', vec_sp=5, vec_idp=2, resource=self.r)
        self.attr2.full_clean()
        self.attr2.save()
    
    def test_get(self):
        self.client.force_authenticate(self.sp_user)
        r = self.client.get(reverse('api:resource_list'))
        self.maxDiff=None
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.json(),[{
            'uuid': str(self.r.uuid),
            'added': self.r.added.astimezone(timezone.get_default_timezone()).isoformat(),
            'updated': self.r.updated.astimezone(timezone.get_default_timezone()).isoformat(),
            'deletion_date': self.r.deletion_date.astimezone(timezone.get_default_timezone()).isoformat(),
            'expiry_date': self.r.expiry_date.astimezone(timezone.get_default_timezone()).isoformat(),
            'eppn': 'foo@b.edu',
            'realm_idp': 'b.edu',
            'setup_token': '1234',
            'sp': str(self.sp.uuid),
            'sp_primary_key': '',
            'attributes': [
                {
                    'name': 'test',
                    'value': 'foo',
                    'default_value': '',
                    'vec_sp': 3,
                    'vec_idp': 1
                },
                {
                    'name': 'other',
                    'value': 'bar',
                    'default_value': '',
                    'vec_sp': 5,
                    'vec_idp': 2
                },
            ]
        }])

    def _run_test_update(self, vec_sp, vec_idp):
        self._run_test_update_with_result(vec_sp, vec_idp, 200, vec_sp, vec_idp, 'updated')

    def _run_test_update_with_result(self, vec_sp, vec_idp, expected_status_code, expected_vec_sp, expected_vec_idp, expected_status):
        data = [
            {
               'name': 'other',
               'value': 'bar2',
               'default_value': '',
               'vec_sp': vec_sp,
               'vec_idp': vec_idp
            },
        ]
        url = reverse("api:resource_attributes", args=(self.r.pk,))
        res = self.client.put(url, data, format='json')
        self.assertEqual(res.status_code, expected_status_code)
        result_data = res.json()
        result_dict = result_data[0]['other']
        self.assertEqual(result_dict, {
            'status': expected_status,
            'vec_sp': expected_vec_sp,
            'vec_idp': expected_vec_idp,
        })
        attr = Attribute.objects.get(pk=self.attr2.pk)
        self.assertEqual({
            'vec_sp': attr.vec_sp,
            'vec_idp': attr.vec_idp,
        }, {
            'vec_sp': expected_vec_sp,
            'vec_idp': expected_vec_idp,
        })
        if vec_sp == expected_vec_sp and vec_idp == expected_vec_idp:
            self.assertEqual(attr.value, 'bar2')
        else:
            self.assertEqual(attr.value, 'bar')

    def test_update_ok_as_sp(self):
        self.assertEqual(self.sp.is_local, True)
        self.client.force_authenticate(self.sp_user)
        self._run_test_update(6, 2)

    def test_update_conflict_as_sp(self):
        """
        Dieses Update muss fehlschlagen, weil es eine Nebenläufigkeit der Vektoruhren gibt.
        """
        self.assertEqual(self.sp.is_local, True)
        self.client.force_authenticate(self.sp_user)
        self._run_test_update_with_result(6, 1, 200, 5, 2, 'rejected')

    def test_update_too_old_as_sp(self):
        """
        Dieses Update muss fehlschlagen, weil der SP Nutzer auf der SP Instanz
        versucht, einen Wert zu aktualisieren, obwohl der mitgeschickte vec_sp
        Zähler zu alt ist
        """
        self.assertEqual(self.sp.is_local, True)
        self.client.force_authenticate(self.sp_user)
        self._run_test_update_with_result(4, 2, 200, 5, 2, 'rejected')

    def test_update_sp_must_not_incement_idp(self):
        """
        Dieses Update muss fehlschlagen, weil der SP den Zähler vom IDP nicht erhöhen darf.
        """
        self.assertEqual(self.sp.is_local, True)
        self.client.force_authenticate(self.sp_user)
        self._run_test_update_with_result(6, 3, 200, 5, 2, 'rejected')
        self._run_test_update_with_result(5, 3, 200, 5, 2, 'rejected')


    @override_settings(SERVER_REALM="b.edu")
    def test_update_ok_as_idp(self):
        self.assertEqual(self.sp.is_local, False)
        self.client.force_authenticate(self.idp_user)
        self._run_test_update(5, 3)

    @override_settings(SERVER_REALM="b.edu")
    def test_conflict_as_idp(self):
        """
        Dieses Update muss fehlschlagen, weil es eine Nebenläufigkeit der Vektoruhren gibt.
        """
        self.assertEqual(self.sp.is_local, False)
        self.client.force_authenticate(self.idp_user)
        self._run_test_update_with_result(4, 3, 200, 5, 2, 'rejected')

    @override_settings(SERVER_REALM="b.edu")
    def test_too_old_as_idp(self):
        """
        Dieses Update muss fehlschlagen, weil der IDP Nutzer auf der IDP Instanz
        versucht, einen Wert zu aktualisieren, obwohl der mitgeschickte vec_idp
        Zähler zu alt ist
        """
        self.assertEqual(self.sp.is_local, False)
        self.client.force_authenticate(self.idp_user)
        self._run_test_update_with_result(5, 1, 200, 5, 2, 'rejected')

    def test_update_idp_must_not_incement_sp(self):
        """
        Dieses Update muss fehlschlagen, weil der IDP den Zähler vom SP nicht erhöhen darf.
        """
        self.assertEqual(self.sp.is_local, True)
        self.client.force_authenticate(self.idp_user)
        self._run_test_update_with_result(6, 3, 200, 5, 2, 'rejected')
        self._run_test_update_with_result(6, 2, 200, 5, 2, 'rejected')

    @override_settings(SERVER_REALM="b.edu")
    def test_ok_sync_from_sp_side(self):
        # Hier testen wir, ob die Vektoruhr richtig gesetzt wird, wenn ein sync
        # update vin der Seite des SPs beim IdP aufgerufen wird, also der SP ein
        # Attribut geändert hat
        self.assertEqual(self.sp.is_local, False)
        self.client.force_authenticate(self.sync_user)
        self._run_test_update(6, 2)

    @override_settings(SERVER_REALM="b.edu")
    def test_conflict_sync_from_sp_side(self):
        self.assertEqual(self.sp.is_local, False)
        self.client.force_authenticate(self.sync_user)
        # nebenläufig
        self._run_test_update_with_result(6, 1, 200, 5, 2, 'rejected')
        # IdP Wert geändert
        self._run_test_update_with_result(6, 3, 200, 5, 2, 'rejected')
        # SP Wert Lokal schon neuer
        self._run_test_update_with_result(4, 1, 200, 5, 2, 'rejected')
        # IdP Wert geändert, SP Wert lokal schon neuer
        self._run_test_update_with_result(4, 3, 200, 5, 2, 'rejected')

    def test_ok_sync_from_idp_side(self):
        """
        hier testen wir, ob die Vektoruhr richtig gesetzt wird, wenn ein sync
        update von der Seite des IdPs beim SP aufgerufen wird, also der IdP
        ein Attribut geändert hat und dass es beim SP richtig aktualisiert wird
        """
        self.assertEqual(self.sp.is_local, True)
        self.client.force_authenticate(self.sync_user)
        self._run_test_update(5, 3)

    def test_conflict_sync_from_idp_side(self):
        """
        Das muss fehlschlagen, weil es hier eine Nebenläufigkeit der Vektoruhren
        gibt.
        """
        self.assertEqual(self.sp.is_local, True)
        self.client.force_authenticate(self.sync_user)
        self._run_test_update_with_result(4, 3, 200, 5, 2, 'rejected')
        self._run_test_update_with_result(6, 3, 200, 5, 2, 'rejected')
        self._run_test_update_with_result(4, 2, 200, 5, 2, 'rejected')
        self._run_test_update_with_result(6, 2, 200, 5, 2, 'rejected')


class AttributesTest(APITestCase):
    def setUp(self):
        self.local_sp = ServiceProvider.objects.create(
            name="TestService",
            expiry_date=timezone.now(),
            realm="a.edu"
        )
        self.sp_user = TokenAuthUser.objects.create_superuser(
            username='sp',
            email='foo@bar.com',
            password='sp',
            user_type=UserType.TYPE_SP,
            api_url="http://api.a.edu:7000/",
            realm="a.edu",
            sp=self.local_sp,
        )

    def test_unauthenticated(self):
        invalid_uuid = str(uuid.uuid4())
        url = reverse("api:resource_attributes", args=(invalid_uuid,))
        r = self.client.get(url)
        self.assertEqual(r.status_code, 401)

    def test_not_exist(self):
        invalid_uuid = str(uuid.uuid4())
        self.client.force_authenticate(self.sp_user)
        url = reverse("api:resource_attributes", args=(invalid_uuid,))
        r = self.client.get(url)
        self.assertEqual(r.status_code, 404)

    def test_regular(self):
        r = Resource(sp=self.local_sp, expiry_date=timezone.now(), deletion_date=timezone.now(), setup_token="1234", eppn="foo@foo.bar")
        r.full_clean()
        r.save()
        self.client.force_authenticate(self.sp_user)
        url = reverse("api:resource_attributes", args=(r.pk,))
        r = self.client.get(url)
        self.assertEqual(r.status_code, 200)


class ServiceProviderGetTest(APITestCase):
    def setUp(self):
        self.local_sp = ServiceProvider.objects.create(
            name="TestService",
            expiry_date=timezone.now(),
            realm="a.edu"
        )
        self.sp_user = TokenAuthUser.objects.create_superuser(
            username='sp',
            email='foo@bar.com',
            password='sp',
            user_type=UserType.TYPE_SP,
            api_url="http://api.a.edu:7000/",
            realm="a.edu",
            sp=self.local_sp,
        )

    def test_unauthenticated(self):
        invalid_uuid = str(uuid.uuid4())
        url = reverse("api:service_detail", args=(invalid_uuid,))
        r = self.client.get(url)
        self.assertEqual(r.status_code, 401)

    def test_not_exist(self):
        invalid_uuid = str(uuid.uuid4())
        self.client.force_authenticate(self.sp_user)
        url = reverse("api:service_detail", args=(invalid_uuid,))
        r = self.client.get(url)
        self.assertEqual(r.status_code, 404)

    def test_regular(self):
        self.client.force_authenticate(self.sp_user)
        url = reverse("api:service_detail", args=(self.local_sp.pk,))
        r = self.client.get(url)
        self.assertEqual(r.status_code, 200)


class CsvUploadViewTest(APITestCase):
    fixtures = ['csv_import_data.json']

    def upload_csv(self, data):
        csv_data = "\n".join([
            "{},{}".format(d[0].eppn, d[1]) for d in data
        ]).encode("utf-8")
        upload = SimpleUploadedFile('data.csv', csv_data, 'text/csv')
        data = {'csv': upload}
        content = encode_multipart('BoUnDaRyStRiNg', data)
        content_type = 'multipart/form-data; boundary=BoUnDaRyStRiNg'
        #print(content)
        r = self.client.post(self.url, content, content_type=content_type)
        return r

    def setUp(self):
        self.user1 = Resource.objects.get(eppn="user1@a.edu")
        self.user2 = Resource.objects.get(eppn="user2@a.edu")
        self.user3 = Resource.objects.get(eppn="user3@a.edu")
        self.user_extern = Resource.objects.get(eppn="user3@b.edu")
        self.idp_user = TokenAuthUser.objects.create_superuser(
            username='api',
            email='foo@bar.com',
            password='api',
            user_type=UserType.TYPE_API,
            api_url="http://api.a.edu:7000/",
            realm="a.edu",
        )
        self.local_sp = ServiceProvider.objects.create(
            name="TestService",
            expiry_date=timezone.now(),
            realm="a.edu"
        )
        self.sp_user = TokenAuthUser.objects.create_superuser(
            username='sp',
            email='foo@bar.com',
            password='sp',
            user_type=UserType.TYPE_SP,
            api_url="http://api.a.edu:7000/",
            realm="a.edu",
            sp=self.local_sp,
        )

        self.url = reverse('api:csv_upload')

    def test_upload_unauthenticated(self):
        now = timezone.now()
        data = (
            (self.user1, '31.12.%s' % now.year),
        )
        r = self.upload_csv(data)
        self.assertEqual(r.status_code, 401)

    def test_unauthorized(self):
        now = timezone.now()
        data = (
            (self.user1, '31.12.%s' % now.year),
        )
        self.client.force_authenticate(self.sp_user)
        r = self.upload_csv(data)
        self.assertEqual(r.status_code, 403)

    def test_modify(self):
        now = timezone.now()
        data = (
            (self.user1, '31.12.%s' % now.year),
        )
        self.client.force_authenticate(self.idp_user)
        r = self.upload_csv(data)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(
            Resource.objects.get(pk=self.user1.pk).expiry_date,
            timezone.make_aware(datetime.strptime("%s-12-31T00:00:00" % now.year, "%Y-%m-%dT%H:%M:%S"))
        )

    def test_do_not_modify(self):
        self.assertEqual(0, QueueJob.objects.count())
        now = timezone.now()
        today = timezone.make_aware(datetime.strptime("{}-{}-{}T00:00:00".format(now.year, now.month, now.day), "%Y-%m-%dT%H:%M:%S"))
        Resource.objects.update(expiry_date=today, deletion_date=today + timedelta(days=30))
        self.user1 = Resource.objects.get(eppn="user1@a.edu")
        u1_ex = self.user1.expiry_date
        data = [
            (res, res.expiry_date.astimezone(timezone.get_default_timezone()).strftime("%d.%m.%Y"))
            for res in Resource.objects.filter(realm_idp=settings.SERVER_REALM)
        ]
        self.client.force_authenticate(self.idp_user)
        r = self.upload_csv(data)
        self.assertEqual(r.status_code, 200)

        self.assertEqual(u1_ex, Resource.objects.get(pk=self.user1.pk).expiry_date)
        self.assertEqual(0, QueueJob.objects.count())

    def test_dont_touch_external_users(self):
        data = (
            (self.user_extern, '31.12.2016'),
        )
        self.client.force_authenticate(self.idp_user)
        r = self.upload_csv(data)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(
            Resource.objects.get(pk=self.user_extern.pk).expiry_date,
            self.user_extern.expiry_date
        )

    def test_trigger_sync_for_external_services(self):
        data = (
            (self.user3, '31.12.2016'),
        )
        self.client.force_authenticate(self.idp_user)
        r = self.upload_csv(data)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(1, QueueJob.objects.count())
        q = QueueJob.objects.first()
        self.assertEqual(q.res_uuid, self.user3.pk)
