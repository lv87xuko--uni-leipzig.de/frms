# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime
from datetime import timedelta
import tempfile
import os

from django.test import TransactionTestCase
from django.conf import settings
from django.core.management import call_command
from django.utils import timezone

from api.models import Resource, QueueJob


class CsvImporterTest(TransactionTestCase):
    fixtures = ['csv_import_data.json']

    def setUp(self):
        self.user1 = Resource.objects.get(eppn="user1@a.edu")
        self.user2 = Resource.objects.get(eppn="user2@a.edu")
        self.user3 = Resource.objects.get(eppn="user3@a.edu")
        self.user_extern = Resource.objects.get(eppn="user3@b.edu")

    def test_do_not_modify(self):
        self.assertEqual(0, QueueJob.objects.count())
        now = timezone.now()
        today = timezone.make_aware(datetime.strptime("{}-{}-{}T00:00:00".format(now.year, now.month, now.day), "%Y-%m-%dT%H:%M:%S"))
        Resource.objects.update(expiry_date=today, deletion_date=today + timedelta(days=30))
        self.user1 = Resource.objects.get(eppn="user1@a.edu")
        u1_ex = self.user1.expiry_date
        with tempfile.NamedTemporaryFile() as tf:
            for res in Resource.objects.filter(realm_idp=settings.SERVER_REALM):
                tf.write("{},{}\n".format(res.eppn,
                                          res.expiry_date.astimezone(timezone.get_default_timezone())
                                          .strftime("%d.%m.%Y")).encode("utf-8"))

            tf.flush()
            os.fsync(tf.fileno())

            call_command("sync_from_csv", csv=tf.name)

        self.assertEqual(u1_ex, Resource.objects.get(pk=self.user1.pk).expiry_date)
        self.assertEqual(0, QueueJob.objects.count())

    def generate_csv_and_run(self, data):
        with tempfile.NamedTemporaryFile() as tf:
            for d in data:
                tf.write("{},{}\n".format(d[0].eppn, d[1]).encode("utf-8"))
            tf.flush()
            os.fsync(tf.fileno())

            call_command("sync_from_csv", csv=tf.name)

    def test_modify(self):
        self.user2.expiry_date = timezone.now()
        self.user2.save()
        now = timezone.now()
        data = (
            (self.user1, '31.12.%s' % now.year),
        )
        self.generate_csv_and_run(data)
        self.assertEqual(
            Resource.objects.get(pk=self.user1.pk).expiry_date,
            timezone.make_aware(datetime.strptime("%s-12-31T00:00:00" % now.year, "%Y-%m-%dT%H:%M:%S"))
        )
        # record not in csv -> lock user, expiry must be in past
        self.assertTrue(
            Resource.objects.get(pk=self.user2.pk).expiry_date <
            timezone.now()
        )

    def test_dont_touch_external_users(self):
        data = (
            (self.user_extern, '31.12.2016'),
        )
        self.generate_csv_and_run(data)
        self.assertEqual(
            Resource.objects.get(pk=self.user_extern.pk).expiry_date,
            self.user_extern.expiry_date
        )

    def test_trigger_sync_for_external_services(self):
        data = (
            (self.user3, '31.12.2016'),
        )
        self.generate_csv_and_run(data)
        self.assertEqual(1, QueueJob.objects.count())
        q = QueueJob.objects.first()
        self.assertEqual(q.res_uuid, self.user3.pk)
