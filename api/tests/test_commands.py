#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals
# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

from django.core.management import call_command
from django.utils.six import StringIO
from django.test import TestCase
import requests
import requests_mock

from api.models import ServiceProvider, TokenAuthUser, UserType


class AuthTestCommandTest(TestCase):
    def setUp(self):
        self.user = TokenAuthUser.objects.create_superuser(username="b_sync",
                                               email="test@b.edu",
                                               password="b_sync",
                                               user_type=UserType.TYPE_SYNC,
                                               api_url="http://api.b.edu:7001/",
                                               realm="b.edu")

    def test_success(self):
        out = StringIO()
        with requests_mock.Mocker() as m:
            m.register_uri("GET", self.user.remote_reverse("api:health"), status_code=200)
            call_command("auth_test", stdout=out)
        self.assertTrue(m.called)
        self.assertEqual(out.getvalue(), "1 passed, 0 failed.\n")

    def test_failed(self):
        out = StringIO()
        with requests_mock.Mocker() as m:
            m.register_uri("GET", self.user.remote_reverse("api:health"), status_code=500)
            call_command("auth_test", stdout=out)
        self.assertTrue(m.called)
        self.assertEqual(out.getvalue(), "0 passed, 1 failed.\n\nFailed:\nb_sync\n")

    def test_connection_failed(self):
        out = StringIO()
        with requests_mock.Mocker() as m:
            m.register_uri("GET", self.user.remote_reverse("api:health"), exc=requests.exceptions.ConnectTimeout)
            call_command("auth_test", stdout=out)
        self.assertTrue(m.called)
        self.assertEqual(out.getvalue(), "0 passed, 1 failed.\n\nFailed:\nb_sync\n")

    def test_misconfigured_user(self):
        self.user.api_url = ""
        self.user.save()
        out = StringIO()
        with requests_mock.Mocker() as m:
            m.register_uri("GET", self.user.remote_reverse("api:health"))
            call_command("auth_test", stdout=out)
        self.assertFalse(m.called)
        self.assertEqual(out.getvalue(), "0 passed, 1 failed.\n\nFailed:\nb_sync\n")


class ChangeTokenCommandTest(TestCase):
    def setUp(self):
        self.user = TokenAuthUser.objects.create_superuser(username="b_sync",
                                               email="test@b.edu",
                                               password="b_sync",
                                               user_type=UserType.TYPE_SYNC,
                                               api_url="http://api.b.edu:7001/",
                                               realm="b.edu")

    def test_change(self):
        out = StringIO()
        token = "1234567890123456789012345678901234567890"
        self.assertNotEqual(self.user.token(), token)
        call_command("change_token", self.user.username, token=token, stdout=out)
        user = TokenAuthUser.objects.get(pk=self.user.pk)
        self.assertEqual(user.token(), token)
        self.assertEqual(out.getvalue(), "Token changed.\n")

    def test_doesn_not_exist(self):
        out = StringIO()
        token = "1234567890123456789012345678901234567890"
        call_command("change_token", "foobar", token=token, stdout=out)
        self.assertEqual(out.getvalue(), "User foobar does not exist\n")


class CreateSpCommandTest(TestCase):
    def test_create_sp(self):
        out = StringIO()
        servicename = "foobar"
        call_command("create_sp", servicename, stdout=out)

        user = TokenAuthUser.objects.get(username="sp_{}".format(servicename))
        sp = ServiceProvider.objects.get(name=servicename)
        self.assertIn("SP-UUID: {}".format(sp.pk), out.getvalue())
        self.assertIn("Token for Service Provider: {}".format(user.token()), out.getvalue())
