#   This file is part of frms.
#
#   frms is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Foobar is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

# coding=utf-8
from rest_framework import serializers

from api.models import Resource, ServiceProvider, Attribute


class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServiceProvider
        fields = ('uuid', 'name', 'realm', 'expiry_date',
                  'default_delete_after_days', 'default_purge_api_data_after_days')


class MultipleAttributeSerializer(serializers.ListSerializer):
    def update(self, instance, validated_data):
        data_mapping = {item['name']: item for item in validated_data}
        attrib_mapping = {attrib.name: attrib for attrib in instance}

        ret = []  # Holds updated instances that must be returned

        for attrib_name, data in data_mapping.items():
            attribute_by_name = attrib_mapping.get(attrib_name, None)

            if attribute_by_name is not None:
                # Do not overwrite local vector clocks
                data.pop("vec_sp", None)
                data.pop("vec_idp", None)

                ret.append(self.child.update(attribute_by_name, data))
            else:
                # Creating a whole new attribute entry would be possible here,
                # but is ignored for the moment.
                pass

        return ret


class AttributeSerializer(serializers.ModelSerializer):
    vec_sp = serializers.IntegerField(required=False)
    vec_idp = serializers.IntegerField(required=False)
    default_value = serializers.CharField(required=False, allow_blank=True)

    class Meta:
        model = Attribute
        fields = ('name', 'default_value', 'value', 'vec_sp', 'vec_idp')
        list_serializer_class = MultipleAttributeSerializer


class ResourceSerializer(serializers.ModelSerializer):
    attributes = AttributeSerializer(many=True, read_only=True)

    class Meta:
        model = Resource
        fields = '__all__'
