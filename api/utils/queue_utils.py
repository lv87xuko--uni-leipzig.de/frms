# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
import logging
import requests

from django.conf import settings
from django.utils.six import BytesIO
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer

from api.models import QueueJob, QueueJobType, Resource, Attribute
from api.serializers import ResourceSerializer, AttributeSerializer
from api.utils.user_utils import extract_idp

logger = logging.getLogger(__name__)


class SyncException(Exception):
    pass


def dispatch_resource_sync(resource):
    json = JSONRenderer().render(ResourceSerializer(resource).data).decode("utf-8")

    # Remote URL can be inferred by the resource's ePPN FQDN-part minus any sub-domains
    dest_realm = extract_idp(resource.eppn)

    new_job = QueueJob(type=QueueJobType.TYPE_RES, obj=json, realm=dest_realm)
    new_job.save()


def sync_resource(sync_user, resource_json):
    url = sync_user.remote_reverse("api:resource_list")
    token = sync_user.token()
    headers = {'Authorization': 'token ' + str(token),
               'Content-type': 'application/json'}

    r = requests.post(url, data=resource_json, headers=headers, timeout=5)

    if r.status_code == 400:
        # status code could mean, resource exists on remote side
        # try to fetch it an compare.
        uuid = json.loads(resource_json)['uuid']
        detail_url = sync_user.remote_reverse("api:resource_detail", args=(uuid,))
        r2 = requests.get(detail_url, headers=headers)
        if r2.status_code == 200:
            logger.info("Resource {} already exists on remote side".format(uuid))
            return
        else:
            msg = "Sync failed: Host responded with {code}, this could mean the host already knows the resource, but fetching of resource failed: {code_fetch}. Message: {msg}".format(
                code=r.status_code,
                code_fetch=r2.status_code,
                msg=r2.content,
            )

            logger.info(msg)
            raise SyncException(msg)
    if not r.status_code == 201:
        logger.debug("Unexpected response: {}".format(r.content))
        raise SyncException("Sync failed: Host responded with {code}".format(code=r.status_code))


def dispatch_attributes_sync(attributes, res_uuid, dates):
    json = JSONRenderer().render(dates + AttributeSerializer(attributes, many=True).data).decode("utf-8")

    res = Resource.objects.get(uuid=res_uuid)
    if res.sp.realm == settings.SERVER_REALM:
        # We are acting as SP here
        dest_realm = res.realm_idp
    else:
        # We are acting as IdP here
        dest_realm = res.sp.realm

    new_job = QueueJob(type=QueueJobType.TYPE_ATT, obj=json, realm=dest_realm, res_uuid=res_uuid)
    new_job.save()


def sync_attributes(sync_user, attributes_json, res_uuid):
    url = sync_user.remote_reverse("api:resource_attributes", args=[res_uuid])
    token = sync_user.token()
    headers = {'Authorization': 'token ' + str(token),
               'Content-type': 'application/json'}

    r = requests.put(url, data=attributes_json, headers=headers, timeout=5)

    if not r.status_code == 200:
        logger.debug("Unexpected response: {}".format(r.content))
        raise SyncException("Sync failed: Host responded with {code}".format(code=r.status_code))
    else:
        # Back-synchronize maxed vv's of each attribute
        stream = BytesIO(r.content)
        data = JSONParser().parse(stream)

        res = Resource.objects.get(uuid=res_uuid)

        for attrib in data:
            keys, values = zip(*attrib.items())
            if values[0]["status"] == "updated":
                obj = Attribute.objects.get(name=keys[0], resource=res)
                obj.vec_sp = values[0]["vec_sp"]
                obj.vec_idp = values[0]["vec_idp"]
                obj.save()


def dispatch_deletion_sync(resource, res_uuid):
    # Remote URL can be inferred by the resource's ePPN FQDN-part minus any sub-domains
    if resource.sp.realm == settings.SERVER_REALM:
        # We are acting as SP here
        dest_realm = resource.realm_idp
    else:
        # We are acting as IdP here
        dest_realm = resource.sp.realm

    new_job = QueueJob(type=QueueJobType.TYPE_DEL, obj=None, realm=dest_realm, res_uuid=res_uuid)
    new_job.save()


def delete_resource(sync_user, res_uuid):
    url = sync_user.remote_reverse("api:resource_detail", args=[res_uuid])
    token = sync_user.token()
    headers = {'Authorization': 'token ' + str(token),
               'Content-type': 'application/json'}

    r = requests.delete(url, headers=headers, timeout=5)

    if not r.status_code in (200, 404):
        logger.debug("Unexpected response: {}".format(r.content))
        raise SyncException("Sync failed: Host responded with {code}".format(code=r.status_code))
