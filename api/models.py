#   This file is part of frms.
#
#   frms is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Foobar is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

# coding=utf-8
from __future__ import print_function

from logging import getLogger
try:
    from urlparse import urljoin
except ImportError:
    from urllib.parse import urljoin
import uuid as uuid

from jsonfield import JSONField

from django.core.exceptions import ValidationError
from django.db import transaction, models
from django.contrib.auth.models import AbstractUser
from django.conf import settings
from django.urls import get_script_prefix
from django.db.models.signals import post_save
from django.dispatch import receiver
from django_choice_object import Choice
from rest_framework.authtoken.models import Token
from rest_framework.reverse import reverse

from api.signals import resource_updated
from api.utils.vector_clock import VersionVector


class PriorityRole(Choice):
    ROLE_SP = "sp", "Service Provider"
    ROLE_IDP = "idp", "Identity Provider"


class UserType(Choice):
    TYPE_API = "api", "API user"
    TYPE_SYNC = "sync", "Sync user"
    TYPE_SP = "sp", "SP user"
    TYPE_IDP = "idp", "IdP user"


class QueueJobType(Choice):
    TYPE_ATT = "att", "Attribute"
    TYPE_RES = "res", "Resource"
    TYPE_DEL = "del", "Deletion"


class QueueJobStatus(Choice):
    STATUS_READY = "rdy", "Ready"
    STATUS_FINISHED = "fsh", "Finished"


class ServiceProviderQuerySet(models.QuerySet):
    def local(self):
        return self.filter(realm=settings.SERVER_REALM)


class ServiceProviderManager(models.Manager):
    def get_queryset(self):
        return ServiceProviderQuerySet(self.model, using=self._db)

    def local(self):
        return self.get_queryset().local()


class ServiceProvider(models.Model):
    uuid = models.UUIDField(verbose_name="UUID",
                            default=uuid.uuid4,
                            unique=True,
                            primary_key=True)
    name = models.CharField(max_length=100)
    description = models.TextField(
        help_text="Verbose service description. Will be exported to website API",
        default="",
        blank=True)
    url = models.CharField(max_length=255,
                           help_text="where do users reach this service?",
                           default="",
                           blank=True)
    realm = models.CharField(verbose_name="Realm", max_length=100)
    expiry_date = models.DateTimeField(help_text="Date until this service will be available")
    default_delete_after_days = models.IntegerField(
        default=90,
        verbose_name="Default delete policy for resources",
        help_text=("keep the resource this number of days in the real world after"
                   " the user account has expired. This value may be overridden"
                   " by a local ResourcePolicy."),
    )
    default_purge_api_data_after_days = models.IntegerField(
        default=7,
        verbose_name="Default Purge policy for resource records",
        help_text=("purge the resource record this number of days in the"
                   " database after the resource has been deleted in the real world"),
    )

    objects = ServiceProviderManager()

    @property
    def is_local(self):
        return self.realm == settings.SERVER_REALM

    class Meta:
        verbose_name = "Service Provider"

    def __str__(self):
        return "{name} ({local})".format(name=self.name,
                                         local=("local" if self.is_local else "notLocal"))

    @property
    def delete_after_days(self):
        try:
            return self.localresourcepolicy.delete_after_days
        except LocalResourcePolicy.DoesNotExist:
            return self.default_delete_after_days

    @property
    def purge_api_data_after_days(self):
        try:
            return self.localresourcepolicy.purge_api_data_after_days
        except LocalResourcePolicy.DoesNotExist:
            return self.default_purge_api_data_after_days


class LocalResourcePolicy(models.Model):
    sp = models.OneToOneField(ServiceProvider, verbose_name="SP", on_delete=models.CASCADE)
    delete_after_days = models.IntegerField(
        default=90,
        verbose_name="Default delete policy for resources",
        help_text=("keep the resource this number of days in the real world"
                   " after the user account has expired. This value may be"
                   " overridden by a local ResourcePolicy."),
    )
    purge_api_data_after_days = models.IntegerField(
        default=7,
        verbose_name="Default Purge policy for resource records",
        help_text=("purge the resource record this number of days in the"
                   " database after the resource has been deleted in the real world"),
    )

class Resource(models.Model):
    uuid = models.UUIDField(verbose_name="UUID",
                            default=uuid.uuid4,
                            unique=True,
                            primary_key=True)
    setup_token = models.TextField()
    eppn = models.CharField(verbose_name=" ePPN",
                            max_length=100,
                            db_index=True)
    realm_idp = models.CharField(verbose_name="Realm (IdP)",
                                 max_length=100,
                                 editable=False)
    added = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    expiry_date = models.DateTimeField(db_index=True)
    deletion_date = models.DateTimeField(db_index=True)
    sp = models.ForeignKey(ServiceProvider, verbose_name="SP", on_delete=models.PROTECT)
    sp_primary_key = models.CharField(max_length=255,
                                      blank=True,
                                      verbose_name="Primary Key of resource in SP software")

    readonly_fields = ("realm_idp",)

    def __str__(self):
        return "{name} ({realm_sp}): {eppn}" \
            .format(eppn=self.eppn, name=self.sp.name, realm_sp=self.sp.realm)

    def save(self, *args, **kwargs):
        from api.utils.user_utils import extract_idp
        self.realm_idp = extract_idp(self.eppn)

        super(Resource, self).save(*args, **kwargs)

    def update_attribute(self, kv_dict, user_type=UserType.TYPE_SP):
        if self.pk is None:
            raise NotImplementedError("Immediate attribute updates are not"
                                      " possible on new instances yet.")

        reserved_date_attr = ("expiry_date", "deletion_date")
        updated_request_data = []
        res = []
        # Since we want to accept expiry/deletion dates as well,
        # we have to manually process them here and ignore
        # them afterwards.
        dates = list(item for item in kv_dict if item["name"] in reserved_date_attr)
        dates_t = {}  # Transformed dates

        try:
            for date in dates:
                # Strip dates from request.data
                kv_dict.remove(date)

                # Transform date into dict in order to process it eventually
                dates_t[date["name"]] = date["value"]
        except ValueError:
            pass

        with transaction.atomic():
            from api.serializers import AttributeSerializer, ResourceSerializer
            from api.utils.queue_utils import dispatch_attributes_sync
            # Validate dates, save them at the end (see end of method)
            res_ser = ResourceSerializer(self, data=dates_t, partial=True)
            res_ser.is_valid(raise_exception=True)
            is_sp_local = self.sp.is_local
            attributes = Attribute.objects.filter(resource=self)

            # this is used to send signal if attribute has changed
            any_attribute_changed = False

            for rmt_attrib in kv_dict:
                attrib_name = rmt_attrib.get("name")
                attrib_dict = {
                    "status": "non-existent",
                    "vec_sp": -1,
                    "vec_idp": -1
                }

                try:
                    local_attrib = attributes.get(name=attrib_name)
                except Attribute.DoesNotExist:
                    res.append({attrib_name: attrib_dict})
                    continue

                # Check if new value equals the old one
                if local_attrib.value == rmt_attrib.get("value"):
                    attrib_dict.update({
                        "status": "unchanged"
                    })
                    res.append({attrib_name: attrib_dict})
                    continue

                if self.reject_request(is_sp_local, local_attrib, rmt_attrib, user_type):
                    attrib_dict.update({
                        "status": "rejected",
                        "vec_sp": local_attrib.vec_sp,
                        "vec_idp": local_attrib.vec_idp,
                    })
                    res.append({attrib_name: attrib_dict})
                else:
                    self.add_updated_attribute(attrib_dict, attrib_name, is_sp_local,
                                               local_attrib, res, rmt_attrib,
                                               updated_request_data, user_type)
                    any_attribute_changed = True

            attr_ser = AttributeSerializer(attributes, data=updated_request_data, many=True)
            attr_ser.is_valid(raise_exception=True)

            attr_obj = attr_ser.save()
            res_ser.save()

            # Dispatch attributes sync procedure in queue
            if user_type != UserType.TYPE_SYNC:
                # we need to dispatch a queue entry only
                # if the realm of the eppn is different from the sp realm
                if self.realm_idp != self.sp.realm:
                    if attr_obj + dates:
                        dispatch_attributes_sync(attr_obj, self.uuid, dates)
            if any_attribute_changed:
                resource_updated.send(sender=self.__class__, resource=self, user_type=user_type)

        return res

    def reject_request(self, is_sp_local, local_attrib, rmt_attrib, user_type):
        local_vec = VersionVector(local_attrib.vec_sp, local_attrib.vec_idp)
        remote_vec = VersionVector(rmt_attrib.get("vec_sp"), rmt_attrib.get("vec_idp"))
        reject_request = False
        if user_type == UserType.TYPE_SYNC:
            if local_vec.concurrent_to(remote_vec) or remote_vec.happened_before(local_vec):
                # As this is represents a conflict situation - depending on prioritization -
                # we might have to reject this request in order to keep ours dominant
                if local_attrib.prio == PriorityRole.ROLE_SP and is_sp_local:
                    # We are SP and this attribute prioritizes SP
                    reject_request = True

                if local_attrib.prio == PriorityRole.ROLE_IDP and not is_sp_local:
                    # We are IdP and this attribute prioritizes IdP
                    reject_request = True
            # jetzt müssen wir noch testen, dass die andere Seite nicht fälschlicherweise unseren Teil der Vektoruhr updatet
            if is_sp_local:
                if remote_vec.sp and remote_vec.sp != local_vec.sp:
                    reject_request = True
            else:
                if remote_vec.idp and remote_vec.idp != local_vec.idp:
                    reject_request = True

        else:  # Local changes only require incrementing our local component
            if remote_vec.idp and remote_vec.sp:
                if local_vec.concurrent_to(remote_vec) or remote_vec.happened_before(local_vec):
                    reject_request = True
            if remote_vec.idp and remote_vec.idp < local_vec.idp:
                reject_request = True
            if remote_vec.sp and remote_vec.sp < local_vec.sp:
                reject_request = True
            if remote_vec.sp and user_type == UserType.TYPE_IDP and remote_vec.sp != local_vec.sp:
                # IDP Darf SP Wert nicht verändern
                reject_request = True
            if remote_vec.idp and user_type == UserType.TYPE_SP and remote_vec.idp != local_vec.idp:
                # SP Darf IDP Wert nicht verändern
                reject_request = True


        return reject_request

    def add_updated_attribute(self, attrib_dict, attrib_name, is_sp_local,
                              local_attrib, res, rmt_attrib,
                              updated_request_data, user_type):
        # Step 1: Increment local clock component if this is a local change
        if user_type != UserType.TYPE_SYNC:
            if is_sp_local:
                # We are acting as SP here
                local_attrib.vec_sp += 1
            else:
                # We are acting as IdP here
                local_attrib.vec_idp += 1

        # Step 2: Merge both clocks
        if user_type == UserType.TYPE_SYNC:
            local_vec = VersionVector(local_attrib.vec_sp, local_attrib.vec_idp)
            remote_vec = VersionVector(rmt_attrib.get("vec_sp"), rmt_attrib.get("vec_idp"))

            (local_attrib.vec_sp, local_attrib.vec_idp) = local_vec.merge(remote_vec)
        updated_request_data.append(rmt_attrib)
        attrib_dict.update({
            "status": "updated",
            "vec_sp": local_attrib.vec_sp,
            "vec_idp": local_attrib.vec_idp
        })
        res.append({attrib_name: attrib_dict})
        local_attrib.save()  # Save local clock updates

    @property
    def owner_managed_by_local_server(self):
        """
        is the resource owner of this resource in this servers realm?
        """
        return self.realm_idp == settings.SERVER_REALM


class Attribute(models.Model):
    name = models.CharField(max_length=100, db_index=True)
    default_value = models.CharField(max_length=100, blank=True)
    value = models.CharField(max_length=100)
    resource = models.ForeignKey(Resource, related_name="attributes", on_delete=models.CASCADE)
    vec_sp = models.IntegerField(verbose_name="VV (SP)", default=1)
    vec_idp = models.IntegerField(verbose_name="VV (IdP)", default=0)
    prio = models.CharField(verbose_name="Priority role",
                            choices=PriorityRole,
                            max_length=5,
                            default="sp")

    class Meta:
        unique_together = (("name", "resource"),)

    def __str__(self):
        return "{res} has {value} {name}"\
               .format(res=self.resource, value=self.value, name=self.name)

    def clean(self):
        if self.name in ("expiry_date", "deletion_date"):
            raise ValidationError("expiry_date and deletion_date are reserved attribute names.")


class TokenAuthUser(AbstractUser):
    user_type = models.CharField(verbose_name="User type", choices=UserType, max_length=5)
    sp = models.ForeignKey(ServiceProvider,
                           verbose_name="SP",
                           blank=True,
                           null=True,
                           help_text="Must be set for type SP!",
                           on_delete=models.PROTECT,
                          )
    realm = models.CharField(verbose_name="Realm",
                             max_length=100,
                             blank=True,
                             help_text="Must be set for type SYNC!")
    api_url = models.CharField(verbose_name="API URL",
                               max_length=100,
                               blank=True,
                               help_text="Must be set for type SYNC!")

    def clean(self):
        if self.user_type == UserType.TYPE_API:
            if TokenAuthUser.objects.filter(user_type=UserType.TYPE_API).count():
                raise ValidationError("There can only be one. One, to rule them all.")

        if self.user_type == UserType.TYPE_SYNC:
            if not self.realm:
                raise ValidationError("You have to provide a realm if your user type is SYNC")
            if not self.api_url:
                raise ValidationError("You have to provide an API URL if your user type is SYNC")

        if self.user_type == UserType.TYPE_SP:
            if not self.sp:
                raise ValidationError("You have to provide a SP if your user type is SP")
            else:
                if not self.sp.is_local:
                    raise ValidationError("SP must be local")

    def remote_reverse(self, *args, **kwargs):
        """
        Resolves url using django reverse using *args, **kwargs
        and adapt it to point to remote frms instance reachable at self.api_url
        """
        r = reverse(*args, **kwargs)
        local_prefix = get_script_prefix()
        if r.startswith(local_prefix):
            r = r[len(local_prefix):]
        return urljoin(self.api_url, r)

    def token(self):
        return Token.objects.get(user_id=self).key


# This code is triggered whenever a new user has been created
# and saved to the database
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created and not kwargs.get('raw', False):
        Token.objects.create(user=instance)


class QueueJob(models.Model):
    type = models.CharField(verbose_name="Job type",
                            choices=QueueJobType,
                            max_length=5)
    obj = JSONField(verbose_name="JSON-serialized object")
    status = models.CharField(verbose_name="Job status",
                              choices=QueueJobStatus,
                              max_length=5,
                              default=QueueJobStatus.STATUS_READY)
    realm = models.CharField(verbose_name="Destination realm",
                             max_length=100)
    res_uuid = models.UUIDField(verbose_name="Resource UUID",
                                null=True,
                                blank=True)  # Only required for TYPE_ATT
    added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{added} {type} {status}"\
               .format(added=self.added, type=self.type, status=self.status)

    class Meta:
        verbose_name = "Queue Job"
