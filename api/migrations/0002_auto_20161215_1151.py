# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='LocalResourcePolicy',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('delete_after_days', models.IntegerField()),
                ('purge_api_data_after_days', models.IntegerField()),
            ],
        ),
        migrations.AddField(
            model_name='serviceprovider',
            name='default_delete_after_days',
            field=models.IntegerField(default=90, help_text=b'keep the resource this number of days in the real world after the user account has expired. This value may be overridden by a local ResourcePolicy.', verbose_name=b'Default delete policy for resources'),
        ),
        migrations.AddField(
            model_name='serviceprovider',
            name='default_purge_api_data_after_days',
            field=models.IntegerField(default=7, help_text=b'purge the resource record this number of days in the database after the resource has been deleted in the real world', verbose_name=b'Default Purge policy for resource records'),
        ),
        migrations.AddField(
            model_name='serviceprovider',
            name='description',
            field=models.TextField(default=b'', help_text=b'Verbose service description. Will be exported to website API', blank=True),
        ),
        migrations.AddField(
            model_name='serviceprovider',
            name='url',
            field=models.CharField(default=b'', help_text=b'where do users reach this service?', max_length=255, blank=True),
        ),
        migrations.AlterField(
            model_name='serviceprovider',
            name='expiry_date',
            field=models.DateTimeField(help_text=b'Date until this service will be available'),
        ),
        migrations.AddField(
            model_name='localresourcepolicy',
            name='sp',
            field=models.OneToOneField(verbose_name=b'SP', to='api.ServiceProvider', on_delete=models.CASCADE),
        ),
    ]
