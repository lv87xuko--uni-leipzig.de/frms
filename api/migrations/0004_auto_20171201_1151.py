# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_auto_20170815_1631'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attribute',
            name='default_value',
            field=models.CharField(max_length=100, blank=True),
        ),
    ]
