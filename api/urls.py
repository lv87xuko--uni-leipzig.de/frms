# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from api import views

urlpatterns = [
    url(r'^$', views.WelcomeView.as_view()),
    url(r'^health/$', views.Health.as_view(), name='health'),
    url(r'^csv_upload/$', views.CsvUploadView.as_view(), name='csv_upload'),
    url(r'^services/$', views.ServiceList.as_view(), name='service_list'),
    url(r'^services/(?P<uuid>[^/]+)/$',
        views.ServiceDetail.as_view(), name='service_detail'),
    url(r'^res/$', views.ResourceList.as_view(), name='resource_list'),
    url(r'^res/(?P<uuid>[^/]+)/$',
        views.ResourceDetail.as_view(), name='resource_detail'),
    url(r'^res/(?P<uuid>[^/]+)/attributes/$',
        views.ResourceAttributes.as_view(), name='resource_attributes')
]

urlpatterns = format_suffix_patterns(urlpatterns)
app_name = 'api'
