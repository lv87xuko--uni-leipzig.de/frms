# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging
import sys

from dateutil.relativedelta import relativedelta
import requests

from django.core.management import BaseCommand
from django.utils import timezone

from api.models import Resource, UserType, TokenAuthUser, QueueJob, QueueJobStatus

logger = logging.getLogger(__name__)


class Command(BaseCommand):

    def handle(self, *args, **options):
        self.set_verbosity(options.get("verbosity"))
        health_ok = self.check_health()
        queue_ok = self.check_queue()
        if health_ok and queue_ok:
            sys.exit(0)
        sys.exit(1)

    def set_verbosity(self, verbosity):
        if verbosity == 0:
            logger.setLevel(logging.WARN)
        elif verbosity == 1:  # default
            logger.setLevel(logging.INFO)
        elif verbosity > 1:
            logger.setLevel(logging.DEBUG)
        if verbosity > 1:
            console = logging.StreamHandler()
            console.setLevel(logging.DEBUG)
            console.setFormatter(logging.Formatter('%(name)s - %(levelname)s - %(message)s'))
            logger.addHandler(console)

    def check_health(self):
        all_ok = True
        for user in TokenAuthUser.objects.filter(user_type=UserType.TYPE_SYNC).exclude(api_url=""):
            token = user.token()
            headers = {'Authorization': 'token ' + str(token)}
            try:
                url = user.remote_reverse("api:health")
                r = requests.get(url, headers=headers, timeout=5)
                if r.status_code == 200:
                    logger.info("{} OK".format(user.username))
                else:
                    logger.error("{} FAILED with HHTP code {}".format(user.username, r.status_code))
                    all_ok = False
            except requests.exceptions.ConnectionError:
                logger.error("{} FAILED with connection error".format(user.username))
                all_ok = False
        return all_ok

    def check_queue(self):
        ten_min_ago = timezone.now() - relativedelta(minutes=10)

        count = QueueJob.objects.filter(added__lt=ten_min_ago)\
            .exclude(status=QueueJobStatus.STATUS_FINISHED).count()
        if count > 0:
            logger.error("There are {} Queue Jobs which are not deployed since 10 Minutes"
                         .format(count))
            return False
        logger.info("There are no deploy jobs older than 10 minutes")
        return True
