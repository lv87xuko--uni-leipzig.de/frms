# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import requests

from django.core.management import BaseCommand
from rest_framework.authtoken.models import Token

from api.models import TokenAuthUser, UserType


class Command(BaseCommand):
    def handle(self, *args, **options):
        successful = []
        failed = []

        for user in TokenAuthUser.objects.all():
            token = Token.objects.get(user=user)
            headers = {'Authorization': 'token ' + str(token)}

            if not user.user_type == UserType.TYPE_SYNC:
                continue  # pragma: no cover

            if not user.api_url:
                failed.append(user.username)
                continue

            try:
                url = user.remote_reverse("api:health")
                r = requests.get(url, headers=headers, timeout=5)

                if r.status_code == 200:
                    successful.append(user.username)
                else:
                    failed.append(user.username)
            except requests.exceptions.ConnectionError:
                failed.append(user.username)

        self.stdout.write("{ok} passed, {fail} failed.".format(ok=len(successful), fail=len(failed)))
        if failed:
            self.stdout.write("\nFailed:")
            for username in failed:
                self.stdout.write(username)
