# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.management import BaseCommand
from rest_framework.authtoken.models import Token

from api.models import TokenAuthUser


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('username')
        parser.add_argument('--token')

    def handle(self, *args, **options):
        try:
            user = TokenAuthUser.objects.get(username=options['username'])
        except TokenAuthUser.DoesNotExist:
            self.stdout.write("User {id} does not exist\n".format(id=options['username']))
            return

        try:
            token = Token.objects.get(user=user)
            token.delete()
        except Token.DoesNotExist:  # pragma: no cover
            pass
        token_obj = Token(user=user, key=options['token'])
        token_obj.save()
        self.stdout.write("Token changed.")
