# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import csv
from io import BytesIO
from urlparse import urljoin
from datetime import date, timedelta, datetime

import requests

from django.core.management import BaseCommand
from django.conf import settings

from rest_framework import status
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from rest_framework.reverse import reverse

from api.utils.user_utils import get_local_token


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("csv", nargs="?", default="test.csv", help="CSV Path")
        parser.add_argument("-d", "--date", nargs="?", default="%d.%m.%Y",
                            help="Custom date format")

    def get_post_url(self, res_uuid):
        return urljoin(settings.HTTP_SERVER_NAME, reverse("resource_attributes", args=[res_uuid]))

    def get_post_data(self, expiry_date):
        data = [
            {
                "name": "expiry_date",
                "value": expiry_date
            }
        ]

        return JSONRenderer().render(data)

    def handle(self, *args, **options):
        path = options["csv"]
        date_format = options["date"]

        get_url = urljoin(settings.HTTP_SERVER_NAME, reverse("resource_list"))

        headers = {'Authorization': 'token ' + get_local_token(),
                   'Content-type': 'application/json'}

        users = []

        r = requests.get(get_url, headers=headers)
        if r.status_code == status.HTTP_200_OK:
            stream = BytesIO(r.content)
            data = JSONParser().parse(stream)

            yesterday = datetime.now() - timedelta(1)

            for entry in data:
                users.append([entry.get("eppn"), entry.get("uuid"), yesterday.isoformat()])
        else:
            exit(0)

        with open(path, 'r') as f:
            entries = csv.reader(f, delimiter=',')
            for entry in entries:
                eppn = entry[0]
                exp_date = datetime.strptime(entry[1], date_format).isoformat()

                users[any(e[0] == eppn for e in users)][2] = exp_date

        for user in users:
            r = requests.put(self.get_post_url(user[1]),
                             headers=headers, data=self.get_post_data(user[2]))
