# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.management import BaseCommand
from django.db import transaction

from api.utils.csv_handling import sync_from_csv_data


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("csv", nargs="?", default="test.csv", help="CSV Path")
        parser.add_argument("-d", "--date", nargs="?", default="%d.%m.%Y",
                            help="Custom date format")

    @transaction.atomic()
    def handle(self, *args, **options):
        path = options["csv"]
        date_format = options["date"]
        with open(path, "rb") as csv:
            sync_from_csv_data(csv, date_format)
