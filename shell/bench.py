# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import grequests

n = 10000
url = "http://192.168.1.222:8000/res/"
headers = {'Authorization': 'token 156d1629c7986df4be39bc292cfab1ec9636a8a4'}

data = {
    "sp": "960ae2a2261340098c47d2c3a9d98f5d",
    "setup_token": "123",
    "eppn": "",
    "expiry_date": "2016-09-15T22:06:04+0200",
    "deletion_date": "2016-09-20T22:06:04+0200",
    "attributes": [
        {
            "name": "DiskQuota",
            "default_value": "0",
            "value": "1"
        }
    ]
}


def exception_handler(request, exception):
    print(exception)

print("Preparing...")
async_list = []

for i in xrange(n):
    data.update({"eppn": "{cnt}@a.edu".format(cnt=i)})
    async_list.append(grequests.post(url, json=data,
                                     headers=headers, timeout=30))

print("Starting.")
grequests.map(async_list, exception_handler=exception_handler,
              gtimeout=30, size=100)
