#!/usr/bin/env bash

API=http://localhost:8001/res/
TOKEN="4c80d439f748aec307378dba06bf21d3ce716268"
SP="53ab67316d1f4a44b8f2e421be2c967d"

DATEFORMAT='+%Y-%m-%dT%T%z'

curl -H "Content-Type: application/json" -H "Authorization: Token $TOKEN" "$API"
