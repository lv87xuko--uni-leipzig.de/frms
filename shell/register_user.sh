#!/usr/bin/env bash

API=http://localhost:8001/res/
TOKEN="4c80d439f748aec307378dba06bf21d3ce716268"
SP="53ab67316d1f4a44b8f2e421be2c967d"

DATEFORMAT='+%Y-%m-%dT%T%z'

user="$1"
realm="$(echo $user|cut -d @ -f 2)"

BODY="$(cat <<-JSON
{
	"setup_token": "1234",
	"eppn": "$user",
	"expiry_date": "$(date -d "+10 days" $DATEFORMAT)",
	"deletion_date": "$(date -d "+14 days" $DATEFORMAT)",
	"sp": "$SP"
}
JSON
)"
echo $BODY
curl -H "Content-Type: application/json" -H "Authorization: Token $TOKEN" -d "$BODY" -X POST "$API"
