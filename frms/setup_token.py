# -*- coding: utf-8 -*-
from __future__ import unicode_literals


def is_valid_setup_token(func):
    def validate(token):
        # ToDo: Implement signature validation
        if token is None:
            # Signature is valid
            func(token)
        else:
            # Signature is invalid
            pass
            # do something here to signalize that
    return validate


@is_valid_setup_token
def parse_setup_token():
    pass
