from django.conf.urls import url, include
from django.contrib import admin


urlpatterns = [
    url(r'^', include('api.urls', namespace='api')),
    url(r'^user/', include('usergui.urls', namespace="usergui")),
    url(r'^admin/', admin.site.urls),
]
