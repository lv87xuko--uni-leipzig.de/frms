# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
from .common import *

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BASE_DIR = os.path.normpath(os.path.join(BASE_DIR, '..'))

HTTP_SERVER_NAME = 'http://192.168.1.123:8000/'
SERVER_REALM = 'a.edu'

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

"""
Bei Bedarf im Entwicklungsmodus zusätzliche Apps laden.
"""
#INSTALLED_APPS.append('debug_toolbar')
#INSTALLED_APPS.append('urztools')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Mails auf die Konsole schreiben, statt sie zu versenden
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# Catchall-Logger für Entwicklungszwecke
# Log-Level kann per Umgebungsvariable `DJANGO_LOG_LEVEL` angepasst werden
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
        },
    },
}

MIDDLEWARE.append('usergui.middleware.DevelopmentUserEppnMiddleware')

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'geqe0rie1_ibza*n311yx53w+eq=9ysv##i07-nfs-d9#o7krl'
