Django>=2.2,<2.3
djangorestframework>=3.7,<3.8
requests==2.20.0
requests_mock
django-choice-object==0.7
jsonfield ==1.0.3
python-dateutil
psycopg2-binary==2.8.4
