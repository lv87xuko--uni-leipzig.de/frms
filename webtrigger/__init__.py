from __future__ import division, print_function, unicode_literals

from django.conf import settings
from django.utils import six
import logging
import requests

from api.signals import resource_created, resource_updated

logger = logging.getLogger(__name__)


def post_resource_event(event_type, resource):
    attributes = {
        attr.name: attr.value for attr in resource.attributes.all()
    }
    attributes["expiry_date"] = resource.expiry_date.strftime('%Y-%m-%dT%H:%M:%SZ')
    attributes["deletion_date"] = resource.expiry_date.strftime('%Y-%m-%dT%H:%M:%SZ')

    data = {
        "event": event_type,
        "details": {
            "service_provider": six.text_type(resource.sp.uuid),
            "eppn": resource.eppn,
            "uuid": six.text_type(resource.uuid),
            "attributes": attributes,
        },
        "version": 1,
    }
    if hasattr(settings, "SAXID_RESOURCE_TRIGGER_HEADERS"):
        r = requests.post(settings.SAXID_RESOURCE_TRIGGER_URL, json=data, headers=settings.SAXID_RESOURCE_TRIGGER_HEADERS)
    else:
        r = requests.post(settings.SAXID_RESOURCE_TRIGGER_URL, json=data)
    if r.status_code != requests.codes.OK:
        logger.warn("Post to {} failed with status {}".format(settings.SAXID_RESOURCE_TRIGGER_URL, r.status_code))
        logger.debug("Post to {} response: {}".format(settings.SAXID_RESOURCE_TRIGGER_URL, r.text))


def _resource_created(sender, resource, user_type, **kwargs):
    if getattr(settings, "SAXID_RESOURCE_TRIGGER_URL", None):
        post_resource_event("create", resource)


def _resource_updated(sender, resource, user_type, **kwargs):
    if getattr(settings, "SAXID_RESOURCE_TRIGGER_URL", None):
        post_resource_event("update", resource)


resource_created.connect(_resource_created, dispatch_uid="webtrigger.resource_created")
resource_updated.connect(_resource_updated, dispatch_uid="webtrigger.resource_updated")
