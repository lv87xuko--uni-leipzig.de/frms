# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

from django.utils import six, timezone
from django.test import override_settings
import requests_mock

from rest_framework.reverse import reverse
from rest_framework.test import APITestCase
from api.models import Attribute, Resource, UserType, TokenAuthUser, ServiceProvider


FAKE_CALLBACK = "http://foo.bar/baz"


@override_settings(SAXID_RESOURCE_TRIGGER_URL=FAKE_CALLBACK)
class ResourceUpdatedTest(APITestCase):
    def setUp(self):
        TokenAuthUser.objects.create_superuser(username="a_super",
                                               email="test@a.edu",
                                               password="a_super",
                                               user_type=UserType.TYPE_API,
                                               api_url="http://api.a.edu:7000/",
                                               realm="a.edu")

        TokenAuthUser.objects.create_superuser(username="b_sync",
                                               email="test@b.edu",
                                               password="b_sync",
                                               user_type=UserType.TYPE_SYNC,
                                               api_url="http://api.b.edu:7001/",
                                               realm="b.edu")

        sp = ServiceProvider.objects.create(name="TestService",
                                            expiry_date=timezone.now(),
                                            realm="a.edu")

        self.res = Resource.objects.create(setup_token="Lorem ipsum",
                                           eppn="u1@b.edu",
                                           expiry_date=timezone.now(),
                                           deletion_date=timezone.now(),
                                           sp=sp)

        self.attrib = Attribute.objects.create(name="Quota",
                                               default_value="100",
                                               value=50,
                                               resource=self.res,
                                               vec_sp=1,
                                               vec_idp=0)

    def _resource_updated_called(self, sender, resource, user_type, **kwargs):
        self.resource_update_signals.append((sender, resource, user_type))

    def test_attribute_update(self):
        url = reverse('api:resource_attributes', args=[self.res.uuid])

        user = TokenAuthUser.objects.get(username='a_super')
        self.client.force_authenticate(user=user, token=user.token())

        data = [
            {
                "name": "Quota",
                "value": "50"
            },
        ]
        with requests_mock.Mocker() as m:
            m.register_uri('POST', FAKE_CALLBACK)
            self.client.put(url, data=data, format='json')

        self.assertFalse(m.called)

        data = [
            {
                "name": "Quota",
                "value": "60"
            },
            {
                "name": "cpuTime",
                "value": 42
            }
        ]

        with requests_mock.Mocker() as m:
            m.register_uri('POST', FAKE_CALLBACK)
            self.client.put(url, data=data, format='json')

        self.assertTrue(m.called)
        posted_data = m.last_request.json()
        wanted_attributes = {
            attr.name: attr.value for attr in self.res.attributes.all()
        }
        wanted_attributes["expiry_date"] = self.res.expiry_date.strftime('%Y-%m-%dT%H:%M:%SZ')
        wanted_attributes["deletion_date"] = self.res.expiry_date.strftime('%Y-%m-%dT%H:%M:%SZ')

        wanted_data = {
            "event": "update",
            "details": {
                "service_provider": six.text_type(self.res.sp.uuid),
                "eppn": self.res.eppn,
                "uuid": six.text_type(self.res.uuid),
                "attributes": wanted_attributes,
            },
            "version": 1,
        }
        self.assertEqual(posted_data, wanted_data)


    @override_settings(SAXID_RESOURCE_TRIGGER_HEADERS={'Authorization': "foobar"})
    def test_additional_headers_used(self):
        url = reverse('api:resource_attributes', args=[self.res.uuid])

        user = TokenAuthUser.objects.get(username='a_super')
        self.client.force_authenticate(user=user, token=user.token())

        data = [
            {
                "name": "Quota",
                "value": "50"
            },
        ]
        with requests_mock.Mocker() as m:
            m.register_uri('POST', FAKE_CALLBACK)
            self.client.put(url, data=data, format='json')

        self.assertFalse(m.called)

        data = [
            {
                "name": "Quota",
                "value": "60"
            },
            {
                "name": "cpuTime",
                "value": 42
            }
        ]

        with requests_mock.Mocker() as m:
            m.register_uri('POST', FAKE_CALLBACK)
            self.client.put(url, data=data, format='json')

        self.assertTrue(m.called)
        posted_data = m.last_request.json()
        wanted_attributes = {
            attr.name: attr.value for attr in self.res.attributes.all()
        }
        wanted_attributes["expiry_date"] = self.res.expiry_date.strftime('%Y-%m-%dT%H:%M:%SZ')
        wanted_attributes["deletion_date"] = self.res.expiry_date.strftime('%Y-%m-%dT%H:%M:%SZ')

        wanted_data = {
            "event": "update",
            "details": {
                "service_provider": six.text_type(self.res.sp.uuid),
                "eppn": self.res.eppn,
                "uuid": six.text_type(self.res.uuid),
                "attributes": wanted_attributes,
            },
            "version": 1,
        }
        self.assertEqual(posted_data, wanted_data)
        self.assertEqual(m.last_request.headers["Authorization"], "foobar")
